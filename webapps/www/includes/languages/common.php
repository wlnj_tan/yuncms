<?php
/** 前端公共语言包 */
$L['msg_bad_data'] = '数据发送自未被信任的域名，如有疑问，请联系管理员';
$L['unit_yun'] = '产品';
$L['unit_pay'] = '港淘';
$L['unit_go_buy'] = '直购';
$L['unit_yun_one'] = '信力包装';
$L['unit_yun_button'] = '立即港淘';
$L['unit_db_points'] = '淘币';
$L['unit_pay_points'] = '经验';
$L['unit_db_scores'] = '福分';
$L['unit_bonus'] = '红包';
$L['unit_baozheng'] = '保证金';
$L['unit_winning'] = '中奖';
$L['unit_price'] = '奖品';
$L['unit_gift'] = '注册';
$L['unit_bonus_unit'] = '个';
$L['unit_transfer'] = '转账';
$L['unit_all_products'] = '全部产品';
$L['unit_a_money_btn'] = '弃奖';
$L['unit_a_money_msg'] = '确定要放弃该奖品吗？';
$this->smarty->assign('L', $this->L=$L);
