<?php
/* *
 * 类名：AlipayNotify
 * 功能：支付宝通知处理类
 * 详细：处理支付宝各接口通知返回
 * 版本：3.2
 * 日期：2011-03-25
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 * 该代码仅供学习和研究支付宝接口使用，只是提供一个参考

 *************************注意*************************
 * 调试通知返回时，可查看或改写log日志的写入TXT里的数据，来检查通知返回是否正常
 */

require_once("alipay_core.function.php");
require_once("alipay_rsa.function.php");

class AlipayNotify {
    /**
     * HTTPS形式消息验证地址
     */
	var $https_verify_url = 'https://mapi.alipay.com/gateway.do?service=notify_verify&';
	/**
     * HTTP形式消息验证地址
     */
	var $http_verify_url = 'http://notify.alipay.com/trade/notify_query.do?';
	var $alipay_config;

	function __construct($alipay_config){
		$this->alipay_config = $alipay_config;
	}
    function AlipayNotify($alipay_config) {
    	$this->__construct($alipay_config);
    }
    /**
     * 针对notify_url验证消息是否是支付宝发出的合法消息
     * @return 验证结果
     */
	function verifyNotify(){
		if(empty($_POST)) {//判断POST来的数组是否为空
			return false;
		}
		else {
			//生成签名结果
			$isSign = $this->getSignVeryfy($_POST, $_POST["sign"]);
			//获取支付宝远程服务器ATN结果（验证是否是支付宝发来的消息）
			$responseTxt = 'false';
			if (! empty($_POST["notify_id"])) {$responseTxt = $this->getResponse($_POST["notify_id"]);}
			
			//写日志记录
			//if ($isSign) {
			//	$isSignStr = 'true';
			//}
			//else {
			//	$isSignStr = 'false';
			//}
			//$log_text = "responseTxt=".$responseTxt."\n notify_url_log:isSign=".$isSignStr.",";
			//$log_text = $log_text.createLinkString($_POST);
			//logResult($log_text);
			
			//验证
			//$responsetTxt的结果不是true，与服务器设置问题、合作身份者ID、notify_id一分钟失效有关
			//isSign的结果不是true，与安全校验码、请求时的参数格式（如：带自定义参数等）、编码格式有关
			if (preg_match("/true$/i",$responseTxt) && $isSign) {
				return true;
			} else {
				return false;
			}
		}
	}
	
    /**
     * 针对return_url验证消息是否是支付宝发出的合法消息
     * @return 验证结果
     */
	function verifyReturn(){
		if(empty($_GET)) {//判断POST来的数组是否为空
			return false;
		}
		else {
			//生成签名结果
			$isSign = $this->getSignVeryfy($_GET, $_GET["sign"]);
			//获取支付宝远程服务器ATN结果（验证是否是支付宝发来的消息）
			$responseTxt = 'false';
			if (! empty($_GET["notify_id"])) {$responseTxt = $this->getResponse($_GET["notify_id"]);}
			
			//写日志记录
			//if ($isSign) {
			//	$isSignStr = 'true';
			//}
			//else {
			//	$isSignStr = 'false';
			//}
			//$log_text = "responseTxt=".$responseTxt."\n return_url_log:isSign=".$isSignStr.",";
			//$log_text = $log_text.createLinkString($_GET);
			//logResult($log_text);
			
			//验证
			//$responsetTxt的结果不是true，与服务器设置问题、合作身份者ID、notify_id一分钟失效有关
			//isSign的结果不是true，与安全校验码、请求时的参数格式（如：带自定义参数等）、编码格式有关
			if (preg_match("/true$/i",$responseTxt) && $isSign) {
				return true;
			} else {
				return false;
			}
		}
	}
	
    /**
     * 获取返回时的签名验证结果
     * @param $para_temp 通知返回来的参数数组
     * @param $sign 返回的签名结果
     * @return 签名验证结果
     */
	function getSignVeryfy($para_temp, $sign) {
		//除去待签名参数数组中的空值和签名参数
		$para_filter = paraFilter($para_temp);
		
		//对待签名参数数组排序
		$para_sort = argSort($para_filter);
		
		//把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
		$prestr = createLinkstring($para_sort);
		
		$isSgin = false;
		switch (strtoupper(trim($this->alipay_config['sign_type']))) {
			case "RSA" :
				$isSgin = rsaVerify($prestr, trim($this->alipay_config['ali_public_key_path']), $sign);
				break;
			default :
				$isSgin = false;
		}
		
		return $isSgin;
	}

    /**
     * 获取远程服务器ATN结果,验证返回URL
     * @param $notify_id 通知校验ID
     * @return 服务器ATN结果
     * 验证结果集：
     * invalid命令参数不对 出现这个错误，请检测返回处理中partner和key是否为空 
     * true 返回正确信息
     * false 请检查防火墙或者是服务器阻止端口问题以及验证时间是否超过一分钟
     */
	function getResponse($notify_id) {
		$transport = strtolower(trim($this->alipay_config['transport']));
		$partner = trim($this->alipay_config['partner']);
		$veryfy_url = '';
		if($transport == 'https') {
			$veryfy_url = $this->https_verify_url;
		}
		else {
			$veryfy_url = $this->http_verify_url;
		}
		$veryfy_url = $veryfy_url."partner=" . $partner . "&notify_id=" . $notify_id;
		$responseTxt = getHttpResponseGET($veryfy_url, $this->alipay_config['cacert']);
		
		return $responseTxt;
	}
	
	/**
	 * 更新订单状态
	 * @param unknown_type $trade_sn 订单ID
	 * @param unknown_type $status 订单状态
	 */
	public function update_recode_status_by_sn($trade_sn,$status) {
	
		$trade_sn = trim($trade_sn);
		$status = trim(intval($status));
		$data = array();
		$account_db = pc_base::load_model('pay_account_model');
	
		$status = $this->return_status($status);
		$data = array('status'=>$status);
		$data1  = array('status'=>$status,'paytime'=>SYS_TIME);
		$order_data = pc_base::load_model('order_model');
		$order = $order_data->get_one(array('ordersn'=>$trade_sn,'status'=>'unpay'),"id,contactid");
	
	
		//$order = $this->order->get_one(array('ordersn'=>$trade_sn),"id,contactid");
	
		if($order){
			$order_data->update($data1,array('ordersn'=>$trade_sn));
			$order_project_data = pc_base::load_model('order_project_model');
			$project_date = pc_base::load_model('project_date_model');
			$project_price = pc_base::load_model('project_price_model');
				
			$project = pc_base::load_model('project_model');
			$member_seller = pc_base::load_model('member_seller_model');
			$member = pc_base::load_model('member_model');
			$order_project = $order_project_data->select(array('orderid'=>$order['id'],'status'=>'unpay'),"title,num,xfnum,tdnum,pid,date,title,code");
		}
		if($status!=0){
			//如果订单不成功，库存返回
			if($order){
				if(!empty($order_project)){
					foreach($order_project as $v){
						$num = $v['num']-$v['xfnum']-$v['tdnum'];
						$project_date->update("stock=stock+$num",array('pid'=>$v['pid'],'date'=>$v['date']));
					}
				}
				$order_project_data->update($data,array('orderid'=>$order['id'],'status'=>'unpay'));
			}
		}else{
	
			if($order){
				$order_project_data->update($data,array('orderid'=>$order['id'],'status'=>'unpay'));
				//发送消息
				//查询会员手机号码
				$contact = pc_base::load_model('contact_model');
				$contact = $contact->get_one(array('id'=>$order['contactid']),'phone,name');
	
				$member_data = pc_base::load_model('member_model');
				if($contact){
					if(!empty($order_project)){
						foreach($order_project as $v){//每一个项目都会发送一条短信
							$num = $v['num']-$v['xfnum']-$v['tdnum'];
							$begintime = date('m月d日',$v['date']);
							$project_date = $project_date->get_one(array('pid'=>$v['pid'],'date'=>$v['date']),'priceid');
							if($project_date['priceid']){
								$project_price = $project_price->get_one(array('id'=>$project_date['priceid']),'validity');
								if($project_price['validity']){
									$endtime = date('m月d日',$project_price['validity']);
									$time = $begintime."至".$endtime;
								}else{
									$time = $begintime;
								}
							}
							$project = $project->get_one(array('id'=>$v['pid']),'sellerid,branch,shmsg,shemail,msg');
							$text = "亲爱的".$contact['name']."，您已购买".$v['title'].$num."张，".$time."有效，凭密码".$v['code']."，".$project['msg'];
							sendmsg(array('0'=>$contact['phone']),$text);
							header("Content-Type: text/html; charset=UTF-8");
							//发送商家短信（1，项目有要求，2，商家有填写）
							$riqi = date('Y-m-d',$v['date']);
	
							if($project['sellerid']){
								$phone = array();
								//查询总店手机号码
								$seller_phone = $member_data->get_one(array('userid'=>$project['sellerid']),'mobile,power');
								if($seller_phone['power']){
									$power_list = string2array($seller_phone['power']);
								}else{
									$power_list = sellpower_list();
								}
								if($seller_phone)$phone[] = $seller_phone['mobile'];
								if($power_list[5]['value']==1)
									$contact_name_sell = $contact['name'];
								else
									$contact_name_sell = substr_replace($contact['name'], "**", 3);
								if($power_list[6]['value']==1)
									$contact_phone_sell = $contact['phone'];
								else
									$contact_phone_sell = substr_replace($contact['phone'], "****", 3,4);
								if($phone&&$project['shmsg']){//如果有填写手机号码才会发送
									$text = "预订人：".$contact_name_sell."，".$contact_phone_sell."，".$v['title']."，数量".$num."张，".$riqi.$project['shmsg'];
									$seller_mobile = $seller_phone['mobile'];
									sendmsg($phone,$text);
									header("Content-Type: text/html; charset=UTF-8");
								}
							}
							if($project['branch']){
								$phone = array();
								//查询发布店铺手机号码
								$branch_phone = $member_data->get_one(array('userid'=>$project['branch']),'mobile,power');
								if($branch_phone['power']){
									$power_list = string2array($branch_phone['power']);
								}else{
									$power_list = sellpower_list();
								}
								if($power_list[5]['value']==1)
									$contact_name_branch = $contact['name'];
								else
									$contact_name_branch = substr_replace($contact['name'], "**", 3);
								if($power_list[6]['value']==1)
									$contact_phone_branch = $contact['phone'];
								else
									$contact_phone_branch = substr_replace($contact['phone'], "****", 3,4);
								if($branch_phone)$phone[] = $branch_phone['mobile'];
								if($seller_mobile!=$branch_phone['mobile']){
									if($phone&&$project['shmsg']){//如果有填写手机号码才会发送
										$text = "预订人：".$contact_name_branch."，".$contact_phone_branch."，".$v['title']."，数量".$num."张，".$riqi.$project['shmsg'];
											
										sendmsg($phone,$text);
										header("Content-Type: text/html; charset=UTF-8");
									}
								}
							}
	
							//$phone = array_unique($phone);
							/* if($phone&&$project['shmsg']){//如果有填写手机号码才会发送
							 $text = "预订人：".substr_replace($contact['name'], "**", 3)."，".substr_replace($contact['phone'], "****", 3,4)."，".$v['title']."，数量".$num."张，".$riqi.$project['shmsg'];
	
							sendmsg($phone,$text);
							header("Content-Type: text/html; charset=UTF-8");
							} */
							//邮箱地址
							$email = array();
							if($project['sellerid']){
								//查询总店邮箱地址
								$seller_email = $member_data->get_one(array('userid'=>$project['sellerid']),'email');
								if($seller_email)$email[0] = $seller_email['email'];
							}
							if($project['branch']){
								//查询分店邮箱地址
								$branch_email = $member_data->get_one(array('userid'=>$project['branch']),'email');
								if($seller_email['email']!=$branch_email['email']){
									if($branch_email)$email[1] = $branch_email['email'];
								}
							}
							if($email&&$project['shemail']){
								$text = array();
								$url = APP_PATH."index.php?m=content&c=index&a=project&id=".$v['pid'];
								$text[0] = "预订人：".$contact_name_sell."，".$contact_phone_sell."，<a href='$url' target='_blank'>".$v['title']."</a>，数量".$num."张，".$riqi.$project['shemail'];
								$text[1] = "预订人：".$contact_name_branch."，".$contact_phone_branch."，<a href='$url' target='_blank'>".$v['title']."</a>，数量".$num."张，".$riqi.$project['shemail'];
								pc_base::load_sys_func('mail');
								foreach($email as $k=>$em){
									sendmail($em, "商户同步推送", $text[$k]);
								}
							}/*
							//手机号码
							$phone = array();
							if($project['sellerid']){
							//查询总店手机号码
							$seller_phone = $member->get_one(array('userid'=>$project['sellerid']),'mobile');
							if($seller_phone)$phone[] = $seller_phone['mobile'];
							}
							if($project['branch']){
							//查询发布店铺手机号码
							$branch_phone = $member->get_one(array('userid'=>$project['branch']),'mobile');
							if($branch_phone)$phone[] = $branch_phone['mobile'];
							}
	
							$phone = array_unique($phone);
							if($phone&&$project['shmsg']){//如果有填写手机号码才会发送
							//$text = "您的项目".$v['title'].",".$riqi.$project['shmsg'];
							$text = "预订人：".$contact['name']."，".substr_replace($contact['phone'], "****", 3,4)."，".$v['title']."，数量".$num."张，".$riqi.$project['shmsg'];
							sendmsg($phone,$text);
							header("Content-Type: text/html; charset=UTF-8");
							}
							//邮箱地址
							$email = array();
							if($project['sellerid']){
							//查询总店邮箱
							$seller_email = $member->get_one(array('userid'=>$project['sellerid']),'email');
							if($seller_email)$email[] = $seller_email['email'];
							}
							if($project['branch']){
							//查询分店邮箱
							$branch_email = $member->get_one(array('userid'=>$project['branch']),'email');
							if($branch_email)$email[] = $branch_email['email'];
							}
							$email = array_unique($email);
							if($email&&$project['shemail']){
							$url = APP_PATH."index.php?m=content&c=index&a=project&id=".$v['pid'];
							//$text = "您的项目<a href='$url' target='_blank'>".$v['title']."</a>,".$riqi.$project['shemail'];
							$text = "预订人：".$contact['name']."，".substr_replace($contact['phone'], "****", 3,4)."，<a href='$url' target='_blank'>".$v['title']."</a>，数量".$num."张，".$riqi.$project['shemail'];
							pc_base::load_sys_func('mail');
							foreach($email as $em){
							sendmail($em, "商户同步推送", $text);
							}
							} */
						}
					}
	
				}
	
			}
		}
		return $account_db->update($data,array('trade_sn'=>$trade_sn));
	}
	function return_status($status) {
		$trade_status = array('0'=>'succ', '1'=>'failed', '2'=>'timeout', '3'=>'progress', '4'=>'unpay', '5'=>'cancel','6'=>'error');
		return $trade_status[$status];
	}
	/**
	 * 更新用户账户余额
	 * @param unknown_type $trade_sn
	 */
	public function update_member_amount_by_sn($trade_sn) {
		$data = $userinfo = array();
		$member_db = pc_base::load_model('member_model');
		$orderinfo = $this->get_userinfo_by_sn($trade_sn);
		$userinfo = $member_db->get_one(array('userid'=>$orderinfo['userid']));
		if($orderinfo){
			$money = floatval($orderinfo['money']);
			$amount = $userinfo['amount'] + $money;
			$data = array('amount'=>$amount);
			return $member_db->update($data,array('userid'=>$orderinfo['userid']));
		} else {
			error_log(date('m-d H:i:s',SYS_TIME).'|  POST: rechange failed! trade_sn:'.$$trade_sn.' |'."\r\n", 3, CACHE_PATH.'pay_error_log.php');
			return false;
		}
	}
	
	/**
	 * 通过订单ID抓取用户信息
	 * @param unknown_type $trade_sn
	 */
	private function get_userinfo_by_sn($trade_sn) {
		$trade_sn = trim($trade_sn);
		$account_db = pc_base::load_model('pay_account_model');
		$result = $account_db->get_one(array('trade_sn'=>$trade_sn));
		$status_arr = array('succ','failed','error','timeout','cancel');
		return ($result && !in_array($result['status'],$status_arr)) ? $result : false;
	}
	
}
?>
