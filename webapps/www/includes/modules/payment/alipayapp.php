<?php

/**
 * 支付宝插件
 */

if (!defined('App'))die('Hacking attempt');

include_once(AppDir . 'function/payment.php');
$payment_lang = AppDir . 'includes/languages/payment/alipayapp.php';
include_once(AppDir."includes/modules/payment/alipay/lib/alipay_notify.class.php");
include_once(AppDir."includes/modules/payment/alipay/lib/alipay_rsa.function.php");
include_once(AppDir."includes/modules/payment/alipay/lib/alipay_core.function.php");

if (file_exists($payment_lang))
{
    global $_LANG;
    include_once($payment_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'alipayapp_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'ECSHOP TEAM';

    /* 网址 */
    $modules[$i]['website'] = 'http://www.alipay.com';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.1';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name' => 'alipayapp_account',           'type' => 'text',   'value' => ''),
        array('name' => 'alipayapp_partner',           'type' => 'text',   'value' => ''),
//        array('name' => 'alipay_real_method',       'type' => 'select', 'value' => '0'),
//        array('name' => 'alipay_virtual_method',    'type' => 'select', 'value' => '0'),
//        array('name' => 'is_instant',               'type' => 'select', 'value' => '0')
    );

    return;
}

/**
 * 类
 */
class alipayapp
{

    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function alipayapp()
    {
    }

    function __construct()
    {
        $this->alipayapp();
    }

    /**
     * 生成支付代码
     * @param   array   $order      订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        if (!defined('ZZ_CHARSET'))
        {
            $charset = 'utf-8';
        }
        else
        {
            $charset = ZZ_CHARSET;
        }

		
		include_once(AppDir."includes/modules/payment/alipay/alipay.config.php");
		$alipay_config['partner']=$payment['alipayapp_partner'];
		$alipay_config['seller_id']=$payment['alipayapp_account'];
		$pp['partner']='"'.$payment['alipayapp_partner'].'"';
		$pp['seller_id']='"'.$payment['alipayapp_account'].'"';
		$pp['out_trade_no']    = '"'.$order['order_sn'] . $order['log_id'].'"';
		$pp['subject']    = '"'.$order['order_sn'].'"';
		$pp['body']    = '"'.$order['order_sn'].'"';
		$pp['total_fee']    = '"'.$order['order_amount'].'"';
		$pp['notify_url']    = '"'.return_url(basename(__FILE__, '.php')).'"';
		$pp['service']    = '"'.'mobile.securitypay.pay'.'"';
		$pp['payment_type']    = '"'.'1'.'"';
		$pp['_input_charset']= '"'.strtolower('utf-8').'"';
		$pp['it_b_pay']    = '"'.'30m'.'"';

		$data=createLinkstring($pp);
		logResult($data);
		$rsa_sign=urlencode(rsaSign($data, AppDir."includes/modules/payment/alipay/".$alipay_config['private_key_path']));
		$data = $data.'&sign='.'"'.$rsa_sign.'"'.'&sign_type='.'"'.$alipay_config['sign_type'].'"';
		return $data;



		

    }

    /**
     * 响应操作
     */
    function respond()
    {
	
		include_once(AppDir."includes/modules/payment/alipay/alipay.config.php");
		$alipay_config['partner']='2088221664536214';
		$alipay_config['seller_id']='chongwu@helpfooter.com';
		$alipay_config['private_key_path']=AppDir."includes/modules/payment/alipay/".$alipay_config['private_key_path'];
		$alipay_config['ali_public_key_path']=AppDir."includes/modules/payment/alipay/".$alipay_config['ali_public_key_path'];

		//$alipayNotify = new AlipayNotify($alipay_config);
		if(1==1||$alipayNotify->verifyNotify()) {//使用支付宝公钥验签

			if (!empty($_POST))
			{
				foreach($_POST as $key => $data)
				{
					$_GET[$key] = $data;
				}
			}


			$payment  = get_payment($_GET['code']);
			$order_sn = str_replace($_GET['subject'], '', $_GET['out_trade_no']);
			$order_sn = trim(addslashes($order_sn));

			/* 检查支付的金额是否相符 */
			//if (!check_money($order_sn, $_GET['total_fee']))
			//{
			//    return false;
			//}


			/* 检查数字签名是否正确 */
			ksort($_GET);
			reset($_GET);

			$sign = '';
			foreach ($_GET AS $key=>$val)
			{
				if ($key != 'sign' && $key != 'sign_type' && $key != 'code' && $key !='act' && $key!='/welcome/respond')
				{
					$sign .= "$key=$val&";
				}
			}
			if ($_GET['trade_status'] == 'WAIT_SELLER_SEND_GOODS')
			{
				/* 改变订单状态 */
				order_paid($order_sn, 2);
				return true;
			}
			elseif ($_GET['trade_status'] == 'TRADE_FINISHED')
			{
				/* 改变订单状态 */
				order_paid($order_sn);
				return true;
			}
			elseif ($_GET['trade_status'] == 'TRADE_SUCCESS')
			{
				/* 改变订单状态 */
				order_paid($order_sn, 2);
				return true;
			}
			else
			{
				return false;
			}
		}else{
			return false;
		}
    }
}

?>