<?php #支付接口函数库

/**
 * 取得返回信息地址
 * @param   string  $code   支付方式代码
 */
function return_url($code)
{
    return RootUrl . 'welcome/respond?code=' . $code;
}

/**
 *  取得某支付方式信息
 *  @param  string  $code   支付方式代码
 */
function get_payment($code)
{
    global $lowxp;

    $sql = "SELECT * FROM ###_payment WHERE pay_code = '$code' AND enabled = '1' LIMIT 1";
    $payment = $lowxp->db->get($sql);

    if ($payment)
    {
        $config_list = unserialize($payment['pay_config']);
        foreach ($config_list AS $config)
        {
            $payment[$config['name']] = $config['value'];
        }
    }

    return $payment;
}

/**
 *  通过订单sn取得订单ID
 *  @param  string  $order_sn   订单sn
 *  @param  blob    $voucher    是否为会员充值
 */
function get_order_id_by_sn($order_sn, $voucher = 'false')
{
    global $lowxp;

    if ($voucher == 'true')
    {
        if($order_sn)
        {
            $lowxp->db->getstr("SELECT log_id FROM ###_pay_log WHERE order_id=" . $order_sn . ' AND order_type='.PAY_SURPLUS);
        }
        else { return ""; }
    }
    else
    {
        $order_id = '';
        if($order_sn)
        {
            $order_id = $lowxp->db->getstr("SELECT id FROM ###_goods_order WHERE order_sn = '$order_sn'");
        }
        if (!empty($order_id))
        {
            $pay_log_id = $lowxp->db->getstr("SELECT log_id FROM ###_pay_log WHERE order_id='" . $order_id . "'");
            return $pay_log_id;
        }
        else { return ""; }
    }
}

/**
 *  通过订单ID取得订单商品名称
 *  @param  string  $order_id   订单ID
 */
function get_goods_name_by_id($order_id)
{
    global $lowxp;

    $sql = "SELECT goods_name FROM ###_goods_order_item WHERE order_id = '".$order_id."'";
    $res = $lowxp->db->select($sql);
    $goods_name = '';
    foreach($res as $v){
        $goods_name .= ','.$v['goods_name'];
    }
    return substr($goods_name, 1);
}

/**
 * 检查支付的金额是否与订单相符
 *
 * @access  public
 * @param   string   $log_id      支付编号
 * @param   float    $money       支付接口返回的金额
 * @return  true
 */
function check_money($log_id, $money)
{
    global $lowxp;

    $sql = "SELECT order_amount FROM ###_pay_log WHERE log_id = '".$log_id."'";
    $amount = $lowxp->db->getstr($sql);

    if ($money == $amount) { return true; }
    else { return false; }
}
/**
 * 插入天工支付订单号
 */
function insert_order_sn($log_id,$order_no){
	global $lowxp;
	if($log_id){
		$lowxp->payment->pay_log_save(array('log_id'=>$log_id,'order_no'=>$order_no));
	}
	return true;
}
/**送充值送红包的临时方案**/
function disacount_ex($discount,$amount)
{
   $disamount=0;
   if(50 < $amount && $amount < 100)
   {
       $disamount=$discount[50];    
   }
   if(100 < $amount && $amount < 200)
   {
        $disamount=$discount[100];    
   }
   else if(200 < $amount && $amount < 500)
   {
        $disamount=$discount[200];    
   }
   else if(500 < $amount && $amount < 1000)
   {
        $disamount=$discount[500];       
   }
   else if(1000 < $amount)
   {
        $disamount=$discount[1000];        
   }
   $disamount = isset($disamount) ? $disamount:0;
   return $disamount;
}
/**
 * 支付订单状态
 */
function order_paid($log_id, $pay_status = PS_PAYED, $note = ''){
    global $lowxp;
    /* 取得支付编号 */
    $log_id = intval($log_id);
    if ($log_id > 0)
    {
        $lowxp->load->model('payment');
        /* 取得要修改的支付记录信息 */
        $pay_log = $lowxp->payment->pay_log($log_id);

        //session记录pay_id
        if($pay_log){
            $_SESSION['pay_id'] = $log_id;
        }

        if ($pay_log && $pay_log['is_paid'] == 0)
        {
            /* 修改此次支付操作的状态为已付款 */
            $lowxp->payment->pay_log_save(array('log_id'=>$log_id,'is_paid'=>1));

            /* 根据记录类型做相应处理 */
            if ($pay_log['order_type'] == PAY_ORDER)
            {
                #订单状态设为：已付款
                $lowxp->load->model('order');
                $lowxp->order->chageOrderState($pay_log['order_id'], 2, '订单支付成功', array('amount'=>$pay_log['order_amount']));

            }elseif($pay_log['order_type'] == PAY_SURPLUS){

                $lowxp->load->model('member');
                $lowxp->db->update('member_account',array('status'=>2),array('id'=>$pay_log['order_id']));
                $member_account = $lowxp->db->get("SELECT * FROM ###_member_account WHERE id = '$pay_log[order_id]'");
                $insert_arr = array();
                $insert_arr['mid'] = $member_account['mid'];
                $insert_arr['user_money'] = $member_account['amount'];
                $insert_arr['desc'] = "通过$member_account[pay_name]充值账户";
                
                //充值送抵用券
                $desc = '充值送'.$lowxp->L['unit_db_points'].$lowxp->L['unit_bonus'];
                $recchage_discount = explode("\n",$lowxp->site_config['recchage_discount']);
                $discount = array();
                if($recchage_discount){
                    foreach($recchage_discount as $v){
                        $v = trim($v);
                        if(empty($v)) continue;

                        $v = explode('|',$v);
                        if(!empty($v[0]) || !empty($v[1])){
                            $discount[trim($v[0])] = trim($v[1]);
                        }
                    }

	                $amount = ceil($member_account['amount']/1);
	                if($amount>9){
		                $give = isset($discount[$amount]) ? $discount[$amount] : disacount_ex($discount,$amount);
		                if($give>0){
		                	$lowxp->load->model('bonus');
		                	$number = ceil($give/1);
		                	$lowxp->bonus->send(array(
		                			'mid'  => $member_account['mid'],
		                			'desc' => $desc,
		                			'number' => 1,
		                			'money' => $number,
		                	));
		                	$insert_arr['desc'] .= "(赠送 $give ".$lowxp->L['unit_db_points'].$lowxp->L['unit_bonus'].")" ;
		                }
	                }
                }

                $lowxp->member->accountlog('recharge',$insert_arr);
                $url = url('/member/accountlog');

            }elseif($pay_log['order_type'] == PAY_DB){
                $lowxp->load->model('yunbuy');
                $lowxp->load->model('member');
                $lowxp->yunbuy->updateOrder(array('status'=>2),$pay_log['order_id']);
                #更新直购订单
                $yun_order_info = $lowxp->yunbuy->orderInfo($pay_log['order_id']);
                $order_info = $lowxp->db->get("SELECT * FROM ###_goods_order WHERE order_sn=".$yun_order_info['order_sn']);
                if($order_info){
                    $lowxp->db->update("goods_order",array('status'=>2,'amount'=>0),array('order_sn'=>$yun_order_info['order_sn']));
                }

                //查询夺宝订单
                $yunorder = $lowxp->yunbuy->orderInfo($pay_log['order_id']);
                //$lowxp->base->write_static_cache('yunorder', $yunorder);
                $bonus['money'] = 0;
                //扣除抵用券
                if($yunorder['user_bonus_id'] > 0){
                    $lowxp->db->save('member_bonus',array(
                        'used_time' => time(),
                        'order_id'  => $pay_log['order_id'],
                    ),'',"id IN(".$yunorder['user_bonus_id'].")");
                    //计算抵用券
                    $bonus['money'] = $lowxp->db->getstr("select SUM(money) from ###_member_bonus where id IN(".$yunorder['user_bonus_id'].")");
                }

                //扣除余额和夺宝币
                if(($yunorder['db_points']+$yunorder['user_money']) > 0){
                    //记录账户明细
                    $log_arr = array();
                    $log_arr['mid'] = $yunorder['mid'];
                    $log_arr['username'] = $yunorder['username'];
                    $log_arr['pay_points'] = -$yunorder['pay_points'];
                    $log_arr['db_points'] = -$yunorder['db_points'];
                    $log_arr['user_money'] = -$yunorder['user_money'];
                    $log_arr['desc'] = $lowxp->L['unit_yun'].'出价 '.$yunorder['order_sn'];
                    
                    if($bonus['money']) $log_arr['desc'] .= "(使用".$lowxp->L['unit_bonus'].$bonus['money']."元)";
                    $log_arr['rank_points'] = $yunorder['pay_points']+$yunorder['user_money']+$yunorder['db_points']; #加经验值
                    $lowxp->member->accountlog('db',$log_arr);
                }

                //在线支付送网盘
                if((($pay_log['order_amonut']+$yunorder['user_money'])>0)&&($lowxp->site_config['pay_discount_status']==1)&&($lowxp->site_config['pay_discount']>0)){
                	//这里增加空间数量
                	$amount = ($pay_log['order_amonut']+$yunorder['user_money'])*number_format($lowxp->site_config['pay_discount']);
                	$lowxp->db->update('member',"spacedata = spacedata+$amount*1024*1024 ",array('mid'=>$yunorder['mid']));
                }

                //记录在线支付实付款记录                    
                $input = array();            
                $input['mid'] = $yunorder['mid'];
                $input['username'] = $yunorder['username'];
                $input['amount'] = $pay_log['order_amount'];                
                $input['pay_id'] = $yunorder['pay_id'];
                $input['pay_name'] = $yunorder['pay_name'];
                $input['pay_code'] = $yunorder['pay_code'];
                $input['user_note'] = $lowxp->L['unit_yun'].'在线支付（'.$yunorder['pay_name'].'）'.$yunorder['order_sn'];
                $input['type'] = 11;
                $input['status'] = 2;
                $state = $lowxp->member->member_account_save($input);

                //按付款金额加经验值
                $desc = "";
               	if($amount){
               		$desc = ",增加"."$amount"."经验值";
               	}
                $lowxp->load->model('member');
                $lowxp->member->accountlog(ACT_DB,array(
                		'mid'=>$yunorder['mid'],
                		'rank_points'=>$pay_log['order_amount'],
                		'amount' => $pay_log['order_amount'],
                		'desc'=>$lowxp->L['unit_yun'].'在线支付（'.$yunorder['pay_name'].'）'.$yunorder['order_sn'].$desc
                ));

                //消费送福分
                $dbmoney = $pay_log['order_amonut']+$yunorder['user_money'];
                if($dbmoney > 0)
                {
                    $member_rank = $this->member->member_rank($_SESSION['mid']);
                    if(!empty($member_rank) && $member_rank['give_db_scores'] > 0)
                    {
                        $dbscores = $dbmoney * $member_rank['give_db_scores'];
                        $this->member->accountlog(ACT_ORDER,array(
                            'mid'        => $yunorder['mid'],
                            'username'   => $yunorder['username'],
                            'db_scores'  => $dbscores,
                            'desc'       => $lowxp->L['unit_yun'].'编码 '.$yunorder['order_sn']."消费".$dbmoney."元送".$dbscores.$this->L['unit_db_scores'],
                        ));
                    }
                }

                $url = url('/member/yunorder');
                $lowxp->yunbuy->orderPayed($pay_log['order_id']);
            }
        }
    }
}
?>