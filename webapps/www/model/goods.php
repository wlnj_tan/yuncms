<?php

/**
 * Class goods_model
 */
class goods_model extends Lowxp_Model{

    private $baseTable = '###_goods';

    function __construct(){}

    /**
     * 商品规格格式化.按spec_item_id排序(从小到大)
     * @param $specstr
     * @return string
     */
    function spec_format($specstr){
        $arr = explode('-',$specstr);
        if(count($arr)){
            foreach($arr as $k=>$val)$arr[$k] = intval($val);
            sort($arr);
            $spec = implode('-',$arr);
        }else{
            $spec = $specstr;
        }
        return $spec;
    }

    /**
     * 获取当个商品
     */
    function get($id, $where='', $field='*'){
        $where = ' WHERE id<>0 '.$where;
        $sql  = "SELECT $field FROM ".$this->baseTable.$where." AND id=".$id;
        $row = $this->db->get($sql);
        if(isset($row['cover']) && $row['cover']){
            $picData = array('id'=>$row['cover']);
            $this->load->model('upload');
            $row['cover'] = $this->upload->getGalleryImgUrls($picData,array('middle','src','thumb'));
        }
        return $row;
    }
    //获取夺宝
    function getgoods($where="",$page_size=10, $page=1, $feild="*",$url='href="/cmsproduct/{num}"'){
        #分页
        $this->load->model('page');
        $_GET['page'] = intval($page);
        $page_size = empty($page_size) ? (int)$this->site_config['page_size'] : $page_size;
        $this->page->set_vars(array('per'=>$page_size,'url'=>$url));
        $where = empty($where) ? "id <> 0" : $where;
        $list = $this->page->hashQuery("SELECT $feild FROM ###_goods WHERE $where")->result_array();

        if($list){
            foreach($list as $key=>$val){
                $val = $this->getThumb($val,1);
                $list[$key] = $val;
            }
        }
        $this->load->model('upload');
        $list = $this->upload->getImgUrls($list,'cover','gallery',array('src'));
        if(!empty($list) && ISAPI==1){
            foreach($list as $key=>$val){
                $list[$key]['imgurl_src'] = $this->upload->thumb($val['imgurl_src'],array('width'=>'200','height'=>'200'));
            }
        }
        return $list;
    }

    /** 重新获取
     * @param array $val 一元数组
     * @param int $type 1返回主图 2返回展示图集
     */
    function getThumb($val,$type=1,$sizeList=array('src')){
        //主图
        $thumb = json_decode($val['thumb'],true);
        $thumbs = json_decode($val['thumbs'],true);
        if($type==2){
            $pics = array();
            foreach($thumbs as $k=>$v){
                $pics[$k]['imgurl_src'] = $v['path'];
            }
            return $pics;
        }else{
            if($thumb[0]['path']){
                $val['imgurl_src'] = $thumb[0]['path'];
            }else{
                $thumbs = json_decode($val['thumbs'],true);
                if($thumbs[0]['path']){
                    $val['imgurl_src'] = $thumbs[0]['path'];
                }
            }
            foreach($sizeList as $size){
                if(isset($val['imgurl_src'])){
                    $val['imgurl_'.$size] = $val['imgurl_src'];
                }
            }
            return $val;
        }
    }

        //获取夺宝详情
    function goodsinfo($id){
        if(empty($id)) return false;
        $row = $this->db->get("SELECT * FROM ###_goods WHERE id = $id");
        if(empty($row)) return false;
        $row = $this->getThumb($row,1,array('middle','src','thumb'));
        #商品图片
        if($row['cover']){
            $picdata = array('id'=>$row['cover']);
            $this->load->model('upload');
            $row['show_cover'] = $this->upload->getGalleryImgUrls($picdata,array('middle','src','thumb'));
        }
        return $row;
    }
}