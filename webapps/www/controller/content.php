<?php
/**
 * ZZCMS 模型内容页
 * ============================================================================
 * * 版权所有 2014-2016 厦门紫竹数码科技有限公司，并保留所有权利。
 * 网站地址: http://www.lnest.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 */
class content extends Lowxp{
    function __construct(){
        parent::__construct();
        $this->load->model('content');
        $this->load->model('category');
        $this->load->model('form_field');
    }

    /* #封面或列表页
     * $data.cat 当前栏目信息
     * $data.row 当前栏目信息 主要用于display_before seo信息提取
     * $data.page 单页内容信息
     * $data.catlist 所有父类ID
     */
    function index($catid,$page=1){
        #栏目信息
        if(!is_numeric($catid)){
            $m = $catid;
        }
        else{
            $data['row'] = $data['cat'] = $this->category->get($catid);
            if(!$data['cat']){ showError('访问错误，栏目不存在');die; }
            $m = $data['cat']['module'];
        }

        $this->chkModule($m);
        $where = ' WHERE lang='.LANG_ID;

        #提交表单
        if(isset($_POST['Submit'])){
            #在线留言同IP一天只能留言一次
            if($m == 'gbook'){
                //$this->exeJs("parent.layer.msg('抱歉，暂不开放留言功能！');");die;
				
                $_POST['post']['ip'] = getIP();
                $sql = "SELECT ip FROM ###_gbook WHERE createtime>'".strtotime('-1 days')."' AND `ip`='".getIP()."'";
                if($this->db->get($sql)){
                    $this->exeJs("parent.layer.msg('24小时内您已留言，请明天再试哦！')");die;
                }
				
                $_POST['post']['name'] = trim($_POST['post']['name']);
                $_POST['post']['email'] = trim($_POST['post']['email']);
                $_POST['post']['content'] = trim($_POST['post']['content']);
				
                if($_POST['post']['content'] == '您希望上架的产品链接' || $_POST['post']['name'] == '您的姓名或昵称' || $_POST['post']['email'] == '您的联系方式'){
                    $this->exeJs("parent.layer.msg('表单填写不完整！');");die;
                }
				
                //有html标签提示
                if($_POST['post']['content']!=strip_tags($_POST['post']['content']) || $_POST['post']['name']!=strip_tags($_POST['post']['name']) || $_POST['post']['email']!=strip_tags($_POST['post']['email'])){
                    $this->exeJs("parent.layer.msg('表单内容不合法，请清除HTML标签！');");die;
                }

                $_POST['post']['name'] = $this->base->safe_replace($_POST['post']['name']);
                $_POST['post']['email'] = $this->base->safe_replace($_POST['post']['email']);
                $_POST['post']['content'] = $this->base->safe_replace($_POST['post']['content']);

                $res = $this->content->save();
            }

            if(isset($res['code']) && $res['code']==0){
                $this->exeJs("parent.layer.msg('留言成功',2,{type:1});");
                $this->refresh(0,true);
            }else{
                $this->exeJs("parent.layer.msg('".$res['message']."')");
            }
            exit;
        }

        #所有父类ID
        $data['catlist'] = $this->category->parentArr($catid);

        #单页
        if($m=='page'){
            $where .= " AND id=".$catid;
            $data['page'] = $this->db->get("SELECT * FROM ###_".$m.$where);

            #更新点击
            if($this->fieldsinfo['hits']){
                $this->db->update($m,'hits=hits+1',array('id'=>$catid));
            }
        }
        #模型列表
        else{
            #内容列表条件
            if($this->fieldsinfo['status']){
                $where .= " AND `status`=1";
            }
            if($catid && $this->fieldsinfo['catid']){
                $where .= " AND `catid`".$this->category->condArrchild($catid);
            }

            #筛选
            if($_GET['act']=='search'){
                $listsearch = array('title','decription','content','catid');
                $conds = $this->content->getConds($listsearch);
                $where .= count($conds) ? ' AND '.implode(' AND ',$conds) : '';
            }

            #内容列表字段
            $fields = $this->moduleinfo['listfields'];
            $fields = empty($fields) ? '*' : $fields;

            #分页
            $this->load->model('page');
            $_GET['page'] = intval($page);
            $pagesize = $data['cat']['pagesize']?(int)$data['cat']['pagesize']:(int)$this->site_config['page_size'];
            $this->page->set_vars(array('per'=>$pagesize));

            #内容列表结果
            $sql = "SELECT * FROM ###_".$m.$where." ORDER BY listorder DESC,id DESC";
            $data['list'] = $this->page->hashQuery($sql,$catid.'/')->result_array();
        }

        #发送变量
        $this->smarty->assign('fieldsinfo',$this->fieldsinfo);
        $this->smarty->assign('data',$data);
        $this->display_before($data['row']);

        #显示模板
        $template = 'list';
        if($data['row']['listtype']==2) $template = 'form';
        if($data['row']['listtype']==1 || $m=='page') $template = 'index';
        if($data['row']['listtype']==0) $template = 'list';

        $template = !empty($data['row']['template_list']) ? $data['row']['template_list'] : $template;

        $this->smarty->assign('page_data_id',isset($catid)?$catid:0);
		
		if(!empty($_GET["js"])){
			$js="no_menu";
		}

        $tpl = $m.'_'.$template.$js.'.html';
        $this->smarty->display($tpl);
    }

  function newsindex($catid,$newid=0){
        #栏目信息
        if(!is_numeric($catid) && !is_numeric($newid)){
           exit(json_encode(array('data'=>'error')));
        }
             #内容列表结果
        $sql = "SELECT * FROM ###_article where catid = ".$catid." and id = ".$newid;
        $data = $this->db->get($sql);
        $data['createtime'] = date('Y-m-d H:i', $data['createtime']); 
        exit(json_encode(array('data'=>$data)));

    }


    /* #详情页
     * $data.cat 所属栏目信息
     * $data.row 内容信息
     * $data.catlist 所有父类ID
     */
    function show($catid,$id){
        #栏目信息
        $data['cat'] = $this->category->get($catid);
        if(!$data['cat']){ showError('访问错误，栏目不存在');die; }
        if(empty($id)){ showError('内容不存在');die; }

        $m = $data['cat']['module'];
        $this->chkModule($m);

        #所有父类ID
        $data['catlist'] = $this->category->parentArr($catid);

        #条件
        $id = (int) $id;
        $where = " WHERE id=$id AND lang=".LANG_ID;
        if($this->fieldsinfo['status']){
            $where .= " AND status=1 ";
        }

        #内容详情与所属栏目
        $data['row'] = $this->db->get("SELECT * FROM ###_".$m.$where);
        if($data['row']){
            #更新点击
            if($this->fieldsinfo['hits']){
                $this->db->update($m,'hits=hits+1',array('id'=>$id));
            }
        }else{ showError('访问错误，内容不存在');die; }

             #分页
            $this->load->model('page');
            $_GET['page'] = intval($page);
            $pagesize = $data['cat']['pagesize']?(int)$data['cat']['pagesize']:(int)$this->site_config['page_size'];
            $this->page->set_vars(array('per'=>$pagesize));
            

            #内容列表结果
            $sql = "SELECT * FROM ###_".$m." WHERE lang=".LANG_ID." AND `catid`".$this->category->condArrchild($catid)." AND status=1 ORDER BY listorder DESC,id DESC";
            $data['list'] = $this->page->hashQuery($sql,$catid.'/')->result_array();

           
        #发送变量
        $this->smarty->assign('fieldsinfo',$this->fieldsinfo);
        $this->smarty->assign('data',$data);
        $this->display_before($data['row']);

        #显示模板
        $template = 'show';
        if(isset($data['row']['template']) && !empty($data['row']['template'])){
            $template = $data['row']['template'];
        }else if(!empty($data['cat']['template_show'])){
            $template = $data['cat']['template_show'];
        }

        $tpl = $m.'_'.$template.'.html';
        $this->smarty->display($tpl);
    }

    #检查模型是否存在 and 获取模型和字段配置信息
    function chkModule($m){
        $this->content->chkModule($m);
        $this->moduleinfo = $this->content->getModuleinfo();
        $this->fieldsinfo = $this->content->getFieldsinfo();

        $this->smarty->assign('moduleinfo',$this->moduleinfo);
    }

 
    #静态页面
    function html($name){
        $template = 'html/'.$name.'.html';
        if(empty($name) || !$this->smarty->templateExists($template)) $this->msg();
        $row = array();

        if($name == 'winrules'){
            $row['title'] = $this->L['unit_pay'].$this->L['unit_winning'].'规则';
        }
        elseif($name == 'js'){
            $content = $this->smarty->fetch($template);
            die($content);
        }elseif($name=='apps'){
			//生成二维码
        	$qrcode = creat_code(url('/content/download/qr.png'));
        	$ios_url = $this->site_config['ios_url'];
        	$and_url = $this->site_config['and_url'];
        	
        	$this->smarty->assign('qrcode',$qrcode);
        	$this->smarty->assign('ios_url',$ios_url);
        	$this->smarty->assign('and_url',$and_url);
        	$row['title'] = "APP下载中心";
        }

        $this->ur_here($row['title']);
        $this->display_before($row);
        $this->smarty->display($template);
    }
	
}