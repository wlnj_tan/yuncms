<?php
/**
 * 挖宝 首页
 */
class cmsproduct extends Lowxp{
    function __construct(){
        parent::__construct();
        $method = $_SERVER['request']['method'];
        //验证登录SIGN（APP使用）
        if(!empty($_REQUEST['mid']) && !empty($_REQUEST['sign']) && empty($_SESSION['mid'])){
            $mid = intval($_REQUEST['mid']);
            $this->load->model('member');
            $member = $this->member->member_info($mid);
            if(!empty($member) && md5($member['mid'].$member['salt'])==$_REQUEST['sign']){
                $this->member->setLogin($member, "APP验证登录");
            }
        }
        //需登录的操作
        $LoginActions = in_array($method, array('checkout','done'));
        if($LoginActions && empty($_SESSION['mid'])){
            $this->msg('',array('url'=>url('/member/login')));
        }
        $this->load->model('goods');
        $this->smarty->assign('navMark', 'yunbuy');
    }

    function index($page=1){
        $this->load->model("goodscat");
        $template = 'index.html';
        $template_mobile = 'lists.html';

        $where = '';
        $cid = isset($_REQUEST['cid']) ? intval($_REQUEST['cid']) : 0;
        $bid = isset($_REQUEST['bid']) ? intval($_REQUEST['bid']) : 0;
        if($cid){
            $cat = $this->db->get("SELECT * FROM goods_cat WHERE id = '$cid'");
            if(!empty($cat)) $where.=" AND cid" . $this->goodscat->condArrchild($cid);
        }
        if($bid){ $where.=" AND bid = '$bid'"; }
        $this->smarty->assign('cid',$cid);
        $this->smarty->assign('bid',$bid);

        $zq = isset($_REQUEST['zq']) ? trim($_REQUEST['zq']) : '';

     /*   //免费专区
        if($type==2){
         //   $title = "免费专区";
        }

        #专区
        if($zq =='limit'){
         //   $where .= " AND b.buynum <> 0";
         //   $title = "限购专区";
        }elseif(intval($zq)){
         //   $where .= " AND b.price = '".intval($zq)."'";
         //   $title = num2char(intval($_REQUEST['zq']))."元专区";
        }
        if($zq =='buy'){
          //  $where .= " AND b.gobuy=1";
            //$title = $this->L['unit_go_buy']."专区";
           // $template = 'index_buy.html';
           // $template_mobile = 'lists_buy.html';
        }else{
          //  $where .= " AND b.gobuy<>1";
        }*/

        $title = !empty($title) ? $title : $this->L['unit_yun_one'];
        #挖宝分类
        $this->load->model('goodscat');
        $yuncat = $this->goodscat->select();
        $this->smarty->assign('yuncat',$yuncat);

        if(STPL == 'mobile'){
            #排序
            $order = ''; //isset($_REQUEST['order']) ? trim($_REQUEST['order']) : '';
            $sort = ''; //isset($_REQUEST['sort']) ? trim($_REQUEST['sort']) : '';
            /*if($order=='count'){
                $order = 'b.goods_click';
            }
            elseif($order=='buy_num'){
                $order = 'b.buy_num';
            }
            else*/
            if($order=='new'){
                $order = 'id';
            }elseif($order=='price'){
                $order = 'price';
            }
           /* elseif($order=='end_num'){
                $order = 'b.end_num';
            }elseif($order=='need_num'){
                $order = 'b.need_num';
            }elseif($order=='custom_price'){
                $order = 'b.custom_price';
            }*/
            elseif(!$order){
                $order = 'listorder DESC, id DESC';
                $sort = '';
            }
            #分页
            $size = !empty($_REQUEST['size']) ? intval($_REQUEST['size']) : 10;
            $this->load->model('page');
            $_GET['page'] = intval($page);
            $this->page->set_vars(array('per'=>$size));

            //搜索
            if(isset($_REQUEST['q']) && !empty($_REQUEST['q'])){
                $this->load->model('auction');
                if($page==1) $this->auction->setSearch($_REQUEST['q']);
                $where .= " AND name LIKE '%". htmlspecialchars(trim($_REQUEST['q'])) ."%'";
            }

            $sql = "SELECT * FROM ###_goods ".
                "WHERE is_sale =1 $where ORDER BY $order $sort";
            $list = $this->page->hashQuery($sql)->result_array();

            $this->load->model('upload');
            $list = $this->upload->getImgUrls($list,'cover','gallery',array('src'));

            if($list){
                foreach($list as $key=>$val){
                    $val = $this->goods->getThumb($val,1);
                    $list[$key] = $val;
                }
            }
            $this->smarty->assign('list',$list);

            #异步加载
            $template_lbi = 'cms/product/lbi/list.html';
            if($_REQUEST['skin']==2){
                $template_lbi = 'cms/product/lbi/list2.html';
            }
            if(isset($_GET['load'])){
                if($list){
                    $content = $this->smarty->fetch($template_lbi);
                    echo $content;die;
                }
            }
            
            $title = '产品中心';
            $row = array('title'=>$title);
            $this->display_before($row);
            $this->smarty->assign('row',$row);
            $this->smarty->display('cms/product/'.$template_mobile);
            die;
        }


        $key = in_array($_GET['k'] , array('id','price')) ? $_GET['k'] : 'listorder';
        $sort = in_array($_GET['s'] , array('desc','asc')) ? $_GET['s'] : 'desc';
        $order = $key." ".$sort;

        $url = 'href="/cmsproduct/index/{num}/?cid='.$cid.'&k='.$_GET['k'].'&s='.$_GET['s'].'&zq='.$_GET['zq'];

        //搜索
        if(isset($_REQUEST['q']) && !empty($_REQUEST['q'])){
            $url .= '&q='.$_REQUEST['q'];
            $this->load->model('auction');
            if($page==1) $this->auction->setSearch($_REQUEST['q']);
            if(isset($_REQUEST['q']) && !empty($_REQUEST['q'])){
                $where .= " AND name LIKE '%". htmlspecialchars(trim($_REQUEST['q'])) ."%'";
            }
        }
        $url .= '"';

        #分页
        $this->load->model('page');
        $_GET['page'] = intval($page);
        $page_size = 20;
        $this->page->set_vars(array('per'=>$page_size,'url'=>$url));

        $sql = "SELECT * FROM ###_goods ".
            "WHERE is_sale=1 $where ".
            "ORDER BY $order";
        $list = $this->page->hashQuery($sql)->result_array();

        if($list){
            foreach($list as $key=>$val){
                $val['imgw'] = 280;
                $val['imgh'] = 195;
                $val = $this->goods->getThumb($val,1);
                $list[$key] = $val;
            }
        }

        $this->load->model('upload');
        $list = $this->upload->getImgUrls($list,'cover','gallery',array('src'));
        $this->smarty->assign('list',$list);

        #敬请期待
        $this->smarty->assign('listNo', 4-count($list)%4);

        $this->smarty->assign('total',$this->page->pages['total']);
        $this->display_before(array('title'=>'产品中心'));
        $this->ur_here($title);
        $this->smarty->display('cms/product/'.$template);
    }

    /**
     * 猜你喜欢
     */
    function randLove(){
        if(!isAjax()) die;
        $zq = isset($_REQUEST['zq']) ? trim($_REQUEST['zq']) : '';
        $where = " AND id!=0";

        $sql = "SELECT * FROM ###_goods WHERE is_sale=1 $where GROUP BY id ORDER BY rand() LIMIT 5";
        $list = $this->db->select($sql);
        foreach($list as $k=>$v){
            $v = $this->goods->getThumb($v);
            $list[$k] = $v;
        }
        $this->smarty->assign('list', $list);
        $content = $this->smarty->fetch('cms/product/lbi_product_love.html');
        echo $content;
    }

    /**
     * 云购详情
     */
    function detail($id='',$op=''){
        $id = intval($id);
        if(empty($id)) $this->msg();
       // $row = $this->goods->goodsinfo($id);
        //if(empty($row)) $this->msg();
        //if($row['is_sale'] == 0){ $this->msg(); }

       
        $templates = 'detail.html';

        #商品来源
        $row = $this->db->get("SELECT * FROM ###_goods WHERE id = '$id'");
        if(empty($row)) $this->msg();
        if($row['is_sale'] == 0){ $this->msg(); }

        $from = explode('|',$row['desc']);
        $this->smarty->assign('from',$from);


        //合并参数
        //$ext_info = unserialize($row['ext_info']);
        //$this->smarty->assign('ext_info', $ext_info);

        #商品详情
        //$row['goods_body'] = $goods['content'];//$this->db->getstr("SELECT content FROM ###_goods WHERE id = '$row[goods_id]'",'content');

        #商品图片
        $thumbs = json_decode($row['thumbs'],true);
        if(!empty($thumbs)){
            $row['pics'] = array();
            foreach($thumbs as $k=>$v){
                $v['imgurl_src'] = $v['path'];
                $row['pics'][$k] = $v;
            }
        }else{
            $row['pics'] = json_decode($row['pics'],true);
            $picdata = array();
            if(is_array($row['pics'])){
                foreach($row['pics'] as $v)$picdata[] = array('id'=>$v);
                $this->load->model('upload');
                $row['pics'] = $this->upload->getGalleryImgUrls($picdata,array('middle','src','thumb'));
            }else{
                $row['pics'] = array('','','','','','');
            }
        }


        if(STPL == 'mobile'){
            $row['head'] = "".$this->L['unit_yun']."详情";
        }

        $this->smarty->assign('row',$row);




        $this->display_before($row);


        #点击量
        //$this->db->update('yunbuy',"goods_click = goods_click+1",array('buy_id'=>$id));

        if(STPL == 'mobile'){
            if($op == 'info'){ //图文详情
                $row['head'] = '图文详情';
                $this->smarty->assign('row',$row);
                $this->smarty->display('cms/product/detail_info.html');
                die;
            }
        }

        $this->smarty->display('cms/product/'.$templates);
    }


   

}