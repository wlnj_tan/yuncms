<?php

/**
 * Class home 首页
 */
class home extends Lowxp{
    function getappversion(){
        $this->api_result(array('version'=>'1.0.1', 'downloadurl'=>'http://www.1yhkt.com/app/download'));
    }

    function getbanner(){
        $id = 18;
        if($_POST['id'] != null && $_POST['id']>0){
            $id = $_POST['id'];
        }
        $this->load->model('upload');
        $ad = $this->db->get("SELECT * FROM ###_ad WHERE id = '$id' AND status = 1");
        $ad['image'] = json_decode($ad['images'],true);

        if(!empty($ad['image'])){
            foreach($ad['image'] as $key=>$val){
                $data = explode(',',$val['title']);
                $parame = array();
                if(!empty($data)){
                    foreach($data as $v){
                        $arr = explode(':',$v);
                        $parame[$arr[0]] = $arr[1];
                    }
                }
                if(!empty($parame['win'])) $ad['image'][$key]['page'] = $parame['win'];
                if(!empty($parame['tab'])) $ad['image'][$key]['tab'] = $parame['tab'];
                $ad['image'][$key]['title'] = json_encode($parame);
                $ad['image'][$key]['orgtitle'] = $val['title'];
                //$ad['image'][$key]['title'] = str_replace('"','\'',json_encode($parame));
                $ad['image'][$key]['path'] = $this->upload->thumb($val['path'],array('width'=>'640','height'=>'200'));
            }
        }
        $this->api_result(array('data'=>$ad['image']));
    }

    function getindexdata(){
        $this->load->model('yunbuy');
        $this->load->model('taglib');
        #获取已揭晓
        //$where = " AND d.status=3 AND d.is_show=1";
        //$data['count'] = $this->yunbuy->getyunDb($where,'count',1);
        $sql = "SELECT * FROM ###_yunbuy WHERE luck_code>0 ORDER BY end_time DESC LIMIT 4";
        $list = $this->db->select($sql);
        $list = $this->db->lJoin($list,'yundb','buy_id,goods_name','buy_id','buy_id');
        $list = $this->db->lJoin($list,'goods','id,price','goods_id','id','g_');
        $list = $this->db->lJoin($list,'member','mid,nickname,photo','member_id','mid');
        $data['luck_db'] = $list;
        if(!empty($data['luck_db'])){
            foreach($data['luck_db'] as $key=>$val){
                $data['luck_db'][$key]['imgurl_thumb'] = $this->taglib->_fileurl(array('source'=>$val['thumb'],'width'=>200,'height'=>200,'type'=>1));
                $data['luck_db'][$key]['username'] = nickname($val['member_name'],$val['nickname']);
                $data['luck_db'][$key]['photo'] = $this->taglib->_photo(array('source'=>$val['photo'],'size'=>30));
            }
        }
        #热门挖宝
        $data['yunbuy'] = $this->yunbuy->getyunbuy("end_num<> 0 AND is_show = 1 ORDER BY listorders DESC,buy_id DESC",6);
        #导航
        $nav = nav(5,5);
        if(!empty($nav)){
            foreach($nav as $key=>$val){
                $link = explode('|',$val['linkurl']);
                $d = explode(',',$link[2]);
                $parame = array();
                if(!empty($d)){
                    foreach($d as $v){
                        $arr = explode(':',$v);
                        $parame[$arr[0]] = $arr[1];
                    }
                }
                $nav[$key]['class'] = $link[0];
                $nav[$key]['color'] = $link[1];
                if(!empty($parame['tab'])){
                    $nav[$key]['tab'] = $parame['tab'];
                    unset($parame['tab']);
                }
                if(!empty($parame['win'])){
                    $nav[$key]['win'] = $parame['win'];
                    unset($parame['win']);
                }
                //$nav[$key]['parame'] = json_encode($parame);
                $nav[$key]['parame'] = str_replace('"','\'',json_encode($parame));
            }
            $data['nav'] = $nav;
        }
        $this->api_result(array('data'=>$data));

    }
    function getsiteconfig(){
        $this->load->model('taglib');
        if($this->site_config['app_logo']!='[]'){
            $this->site_config['app_logo'] = $this->taglib->_fileurl(array('source'=>$this->site_config['app_logo']));
        }else{
            $this->site_config['app_logo']='';
        }
         if(!empty($_SESSION['username']) && intval($_SESSION['mid'])){
              $this->site_config['IsLogined'] = 1;
        }else{
            $this->site_config['IsLogined'] = 0;
        }
        $this->api_result(array('data'=>$this->site_config));
    }

    function errorlog(){
        if(!empty($_POST['log'])){
            $log = $_POST['log'];
            $destination = CLog::$defaultConfig['log_path'] . 'APP_'.date('y_m_d') . '.log';
            $log .= "\r\nUseragent: ".$this->useragent->agent_string();
            CLog::write($log.''.$this->useragent->agent_string(), 'ERR', '', $destination);
            $this->api_result(array('code'=>0, 'log'=>$log));
        }
    }
    
    function addappusagelog(){
        if(!empty($_POST['platform'])){
            $id = $this->db->save('app_log',array(
                    'user_id'   => isset($_SESSION['mid'])?$_SESSION['mid']:0,
                    'network'   => !empty($_POST['network'])?$_POST['network']:'',
                    'ip_address'   => getIP(),
                    'pfm_version'   => !empty($_POST['pfm_version'])?$_POST['pfm_version']:'',
                    'app_version'   => !empty($_POST['app_version'])?$_POST['app_version']:'',
                    'manufacturer'   => !empty($_POST['manufacturer'])?$_POST['manufacturer']:'',
                    'model'   => !empty($_POST['model'])?$_POST['model']:'',
                    'uuid'      => !empty($_POST['uuid'])?$_POST['uuid']:'',
                    'platform' => !empty($_POST['platform'])?$_POST['platform']:'',
                    'log_time' => time(),
                ));
        if(empty($id)){
            return array('code'=>10002, 'message'=>'数据操作失败!');
        } //未知错误

            $this->api_result(array('code'=>0));
        }
    }
}