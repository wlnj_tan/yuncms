<?php
/**
 * ZZCMS 模型内容页
 * ============================================================================
 * * 版权所有 2014-2016 厦门紫竹数码科技有限公司，并保留所有权利。
 * 网站地址: http://www.lnest.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 */
class test extends Lowxp{
    function __construct(){
        parent::__construct();
       
    }

   
    /**
     * 第三方登录
     */
    function oauth(){

        $type = !empty($_REQUEST['type']) ? trim($_REQUEST['type']) : 'qq';
        $oauthcode = trim($_REQUEST['oauthcode']);
        $userid = trim($_REQUEST['userid']);
        $oauth = array();
        if(empty($oauthcode)) exit($this->api_result(array('msg'=>'授权失败, oauth Code不存在')));
        if($type == 'wx'){
            $ret = $this->checkWXAccessTokenForAPI($oauthcode);
            if(!$ret['success']){
                exit($this->api_result(array('msg'=>$ret['msg'])));
            }
            $oauth = $ret['oauth'];
        }
        else{
            //QQ
            $ret = $this->getQQUserInfoForAPI($oauthcode, $userid);
             if(!$ret['success']){
                exit($this->api_result(array('msg'=>$ret['msg'])));
            }
            $oauth = $ret['oauth'];
        }
        
        $data = array();
        $data['oauth'] = $oauth;
        $this->api_result(array('data'=>$data, 'code'=>1));
    }
    function lb(){
        
            $array['list'] = $this->db->lJoin($array['list'],'goods','id,price','goods_id','id','g_');
            exit(json_encode(array('error'=>1,'ids'=>$ids,'data'=>$array)));
    }
}