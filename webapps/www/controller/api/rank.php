<?php

/**
 * Class home 首页
 */
class rank extends Lowxp{

    function index(){
		$this->rankdata("用户消费榜",'consume_rank', '每个月（1号0时至月底最后1天24时）按照当月用户消费的金额进行排名，前三分别获得当月消费总额的6%，5%，4%到账户余额，最高封顶分别是5000，3000，2000,第四到第十获得当月消费额总额的2%到账户余额里面最高限额1000，前提是必须月总消费满2000');
	}
	
    function proxy(){
		$this->rankdata("代理消费榜",'proxyconsume_rank', '每个月（1号0时至月底最后1天24时）按照当月代理消费总额进行排名，前三名分别获得当月代理消费总额的1.5%，1%，0.5%奖励');
	}

    function commission(){
		$this->rankdata("当月返利榜",'commission_rank', '每个月（1号0时至月底最后1天24时）按照当月返利总额进行排名，前三名分别获得不同的神秘礼物');
	}

    private function rankdata($title, $table, $rulescontent){
		$data = array();
		$this->load->model('taglib');
		$data['title'] = $title;
		$data['rulescontent'] = $rulescontent;
		$year=date("Y");
		$month=date("m");

		$sql="select a.*,b.username, b.nickname, b.mid, b.photo from $table a
		inner join member b on a.mid=b.mid
		 where year=$year and month=$month order by rank limit 0,10";
		$list = $this->db->select($sql);

		for($i=0;$i<count($list);$i++){
			$list[$i]["username"] = nickname($list[$i]["username"],$list[$i]["nickname"]);
			$list[$i]["updated_date"] = date('Y-m-d H:i:s',$list[$i]["updated_date"]);
			$list[$i]["photo"] = $this->taglib->_photo(array('source'=>$list[$i]["photo"],'size'=>80));
			$list[$i]["cost"] = price_format($list[$i]["cost"]);
			//$list[$i]["username"]=$this->blurname($list[$i]["username"]);
			//price_format
		}
		$data['list'] = $list;
		$this->api_result(array('data'=>$data));
	}

	function blurname($name){
		$ret="";
		$len=strlen($name);
		$fix=2;
		if($len>6){
			$fix=3;
		}
		for($i;$i<$len;$i++){
			if($i<$fix||$i>=$len-$fix){
				$ret.=$name[$i];
			}else{
				$ret.="*";
			}
		}
		return $ret;
	}
}