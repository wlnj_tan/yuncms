<?php
/**
 * Class home 首页
 */
class activity extends Lowxp{
    function index(){
        if(STPL == 'mobile'){
            $row['head'] = '新闻资讯';
            $this->smarty->assign('row',$row);
        }
        
        $sql = "SELECT * FROM ###_article WHERE lang=1 AND `status`=1 AND `catid` IN(65) ORDER BY listorder DESC,id DESC";

        $list=$this->db->select($sql);
        foreach($list as $key=>$val){
                $list[$key]['content'] = substr(strip_tags($val['content']), 0, 100); //substr_replace(strip_tags($val['content']), '...', 30); 
            }

        $this->smarty->assign("list",$list);
        $this->smarty->display("activity.html");
    }
}