<?php
/**
 * Class welcome
 */
class welcome extends Lowxp{

    /**
     * IndexAction
     */
    function index(){
        echo '<div style="font-family:\5FAE\8F6F\96C5\9ED1;font-size:24px;padding:10px;">欢迎光临'.$this->site_config['site_name'].' :)</div>';
    }

    function setImgSize(){
        return;
        $rows = $this->db->select("SELECT * FROM images");

        foreach($rows as $val){
            $path = RootDir.'web/upload/images/'.$val['cate'].'/'.$val['imgurl'];
            if(is_file($path)){
                $data = getimagesize($path);
                #echo '<pre>';print_r($data);echo '</pre>';
                $size = $data[0].'x'.$data[1];
                $this->db->update('images',array('size'=>$size),array('id'=>$val['id']));
            }
        }
    }

    /**
     * 算术验证码
     */
    function scode() {
        //生成验证码图片
        Header("Content-type: image/PNG");
        srand((double)microtime()*1000000);
        $w = 95; //宽
        $h = 34;  //高

        $answer='';
        $authnum=0;
        $randnuml = rand(1,50);
        $randnumr = rand(1,50);
        $chars = array('+','-');
        $char = array_rand($chars,1);
        $char = $chars[$char];
        switch($char){
            case '+':
                $authnum = $randnuml+$randnumr;
                break;
            case '-':
                $randnumr = rand(0,$randnuml-1);
                $authnum = $randnuml-$randnumr;
                break;
            default:break;
        }
        $answer=$randnuml.$char.$randnumr.'=?';

        $_SESSION['icode'] = iconv('gbk','utf-8',$authnum);

        $im = imagecreate($w,$h);
        $gray = imagecolorallocate($im, rand(230,255),rand(230,255),rand(230,255));
        imagefill($im, 0,0, $gray);

        /*创建干扰线等*/
        for($i=0;$i<10;$i++){
            $color=imagecolorallocate($im,mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));
            imagearc($im,mt_rand(-10,$w),mt_rand(-10,$h),mt_rand(30,300),mt_rand(20,200),55,44,$color);
        }

        for($i=0;$i<20;$i++){
            $color = imagecolorallocate($im, rand(0,255), rand(0,255), rand(0,255));
            imagesetpixel($im, rand()%90, rand()%30, $color);
        }

        imagettftext($im, 14, 10, 10, 34, imagecolorallocate($im, rand(0,128), rand(0,128), rand(0,128)),RootDir.'web/admin/fonts/scode.ttf', strtoupper($answer));
        imagepng($im);
        imagedestroy($im);
    }

    /**
     * 扫码同步登录二维码
     */
    function wx_scan(){
        include AppDir.'library/phpqrcode/phpqrcode.php';
        $errorCorrectionLevel = 'L';//容错级别
        $matrixPointSize = 8;//生成图片大小
        $dir = RootDir.'web/upload/images/qrcode/';
        //生成二维码图片
        $url = RootUrl.session_id();
        QRcode::png($url,false,$errorCorrectionLevel,$matrixPointSize,2);
    }

    /** TODO:短信验证码获取入口
     * verify_code status字段说明（后续添加其它验证码需要往下扩展说明）
     * 1注册验证状态 2注册成功状态
     * 3
     */
    function sms(){
        $iTime = 60; #第ip第隔多少秒才能获取一次

        //总开关
        if(!$this->site_config['sms_open']){
            $result['error'] = 1;
            $result['message'] = '短信接口未开启!';
            die(json_encode($result));
        }

        $mobile = isset($_POST['mobile'])?trim($_POST['mobile']):'';
        $act = isset($_POST['act'])?trim($_POST['act']):'';
        if(empty($act)) die;

        //后台登录验证码
        $sms_manage = 0;
        if($act == 'sms_manage'){
            $sms_manage = 1;

            if(!$this->site_config['managesms']){
                $result['error'] = 1;
                $result['message'] = '后台登录短信验证码功能未开启!';
                die(json_encode($result));
            }

            $username = isset($_POST['username'])?$_POST['username']:'';
            $password = isset($_POST['password'])?$_POST['password']:'';
            if(!$username || !$password){
                $result['error'] = 1;
                $result['message'] = '请输入管理员用户名和密码!';
                die(json_encode($result));
            }else{
                $user = $this->db->get("SELECT * FROM ###_m_user WHERE username='".$username."'");
                $mobile = $user['tel'];
                if(empty($mobile)){
                    $result['error'] = 1;
                    $result['message'] = '该管理员未设置手机号，可直接登录!';
                    die(json_encode($result));
                }
                if(get_salt_hash($password, $user['salt']) != $user['password']){
                    $result['error'] = 1;
                    $result['message'] = '管理员用户名或密码错误!';
                    die(json_encode($result));
                }
            }
        }

        //手机号为空时发送会员手机号
        if(empty($mobile) && $_SESSION['mid']){
            $this->load->model('member');
            $member = $this->member->member_info($_SESSION['mid']);
            $mobile = $member['mobile'];
        }

        //模板开关
        if(!statusTpl($act) && !$sms_manage){
            $result['error'] = 1;
            $result['message'] = '短信模板已禁用!';
            die(json_encode($result));
        }

        //提交的手机号是否正确
        if (empty($mobile) || ($this->base->validate($mobile, 'mobile') == false))
        {
            $result['error'] = 1;
            $result['message'] = '请填写正确的手机号!';
            die(json_encode($result));
        }

        //需要重新设置的变量
        $status = 1;

        //注册验证码
        if($act == 'sms_register'){
            $status = 1;

            //提交的手机号是否已经注册帐号
            $sql = "SELECT COUNT(mid) FROM ###_member WHERE mobile = '$mobile'";
            if ($this->db->getstr($sql) > 0)
            {
                $result['error'] = 2;
                $result['message'] = '手机号已经被注册过!';
                die(json_encode($result));
            }
        }
        //身份证验证
        elseif($act == 'sms_idcard'){
            $status = 3;
        }
        //银行卡验证
        elseif($act == 'sms_bankcard'){
            $status = 4;
        }
        //支付密码验证
        elseif($act == 'sms_chpaypass'){
            $status = 5;
        }
        elseif($act == 'sms_verifymobile'){
            $status = 6;
        }
        elseif($act == 'sms_password'){
            $status = 7;

            //提交的手机号是否已经注册帐号
            $sql = "SELECT COUNT(mid) FROM ###_member WHERE mobile = '$mobile'";
            if ($this->db->getstr($sql) <= 0)
            {
                $result['error'] = 2;
                $result['message'] = '该手机号未注册过!';
                die(json_encode($result));
            }
        }
        elseif($sms_manage){

        }
        else{ die; }

        //同操作同IP超过5次后禁止发送(防刷)
        //注册验证时校对验证码
        if(in_array($act, array('sms_register','sms_password','sms_verifymobile','sms_manage'))){
            if($sms_manage && !$this->site_config['verify_admin']){}
            else{
                $this->load->model('code');
                $res = $this->code->check();
                if($res['code'] != 0){
                    die(json_encode(array('error'=>3,'message'=>$res['msg'])));
                }
            }
        }

//        if(!empty($_SESSION['send_time']) && $_SESSION['send_time']+$iTime <= RUN_TIME){
//            $result['error'] = 3;
//            $result['message'] = '恶意请求';
//            die(json_encode($result));
//        }else{
//            $_SESSION['send_time'] = RUN_TIME;
//        }

        /*$sql = "SELECT COUNT(id) FROM ###_verify_code WHERE status='$status' AND (getip='".getIP()."' OR mobile='$mobile')";
        if ($this->db->getstr($sql) > 3)
        {
            $result['error'] = 3;
            $result['message'] = '恶意请求';
            die(json_encode($result));
        }*/

        //获取验证码请求是否获取过
        if(!$sms_manage){
            $sql = "SELECT COUNT(id) FROM ###_verify_code WHERE status='$status' AND mobile='$mobile' AND getip='".getIP()."' AND dateline>'".(time()-$iTime)."'";
            if ($this->db->getstr($sql) > 0)
            {
                $result['error'] = 3;
                $result['message'] = '每IP每手机号每'.$iTime.'秒只能获取一次验证码。';
                die(json_encode($result));
            }
        }else{
            $act = 'sms_register';
        }

        //获取验证码
        if($_SESSION['voiceVerify']) unset($_SESSION['voiceVerify']);
        $this->load->library('sms');
        $verifycode = $this->sms->getVerifyCode();
        $this->smarty->assign('verify_code',  $verifycode);

        //发送短信
        $ret = $this->sms->sendSmsTpl($mobile, $act);
        if($ret === true)
        {
            $data = array(
                'mobile'     => $mobile,
                'getip'      => getIP(),
                'verifycode' => $verifycode,
                'dateline'   => time(),
                'status'     => $status,
            );
            $this->db->save('###_verify_code',$data);

            $result['error'] = 0;
            $result['message'] = '手机短信验证码发送成功';
            die(json_encode($result));
        }
        else
        {
            $result['error'] = 4;
            $result['message'] = $ret?$ret:'手机短信验证码发送失败!';
            die(json_encode($result));
        }
    }

    /**
     * 语音验证码入口
     */
    function voice(){
        $iTime = 60; #第ip第隔多少秒才能获取一次
        //手机号为空时发送会员手机号
        if($_SESSION['mid']){
            $this->load->model('member');
            $member = $this->member->member_info($_SESSION['mid']);
            $mobile = $member['mobile'];
        }else{
            $mobile = isset($_REQUEST['mobile'])?trim($_REQUEST['mobile']):'';
        }
        //提交的手机号是否正确
        if (empty($mobile) || ($this->base->validate($mobile, 'mobile') == false))
        {
            $result['error'] = 1;
            $result['message'] = '请填写正确的手机号!';
            die(json_encode($result));
        }
        if(!$_SESSION['mid']){
            //提交的手机号是否已经注册帐号
            $sql = "SELECT COUNT(mid) FROM ###_member WHERE mobile = '$mobile'";
            if ($this->db->getstr($sql) > 0)
            {
                $result['error'] = 2;
                $result['message'] = '手机号已经被注册过!';
                die(json_encode($result));
            }
        }
        $status = 10;

        //获取验证码请求是否获取过
        $sql = "SELECT COUNT(id) FROM ###_verify_code WHERE status='$status' AND mobile='$mobile' AND getip='".getIP()."' AND dateline>'".(time()-$iTime)."'";
        if ($this->db->getstr($sql) > 0)
        {
            $result['error'] = 3;
            $result['message'] = '每IP每手机号每'.$iTime.'秒只能获取一次验证码。';
            die(json_encode($result));
        }

        $verifycode = voiceVerify($mobile);
        if($verifycode){
            $data = array(
                'mobile'     => $mobile,
                'getip'      => getIP(),
                'verifycode' => $verifycode,
                'dateline'   => time(),
                'status'     => $status,
            );
            $this->db->save('###_verify_code',$data);
            $result['error'] = 0;
            $result['message'] = '语音验证码发送成功';
            die(json_encode($result));
        }else{
            $result['error'] = 4;
            $result['message'] = '语音验证码发送失败!';
            die(json_encode($result));
        }

    }

    /**
     * 支付返回地址
     */
    function respond($order_sn=""){
	
		$r="\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n".$_SERVER["REQUEST_URI"];;
		$r.="\r\n============================\r\nGET:";
		foreach($_GET as $key => $value ){
			$r.="&$key=$value";
		}
		$r.="\r\n============================\r\nPOST:";
		foreach($_POST as $key => $value ){
			$r.="&$key=$value";
		}
		error_log($r,3,"payreques".date("Y-m-d").".log");



        include_once(AppDir.'function/payment.php');
        if(!empty($_REQUEST['metadata'])){
            $metadata = explode(':',$_REQUEST['metadata']);
            $code = $metadata[2];
        }
        /* 支付方式代码 */
        if(!empty($_REQUEST['code'])){
            $pay_code = trim($_REQUEST['code']);
        }elseif(isset($code)){
            $pay_code = $code;
        }else{
            $pay_code = 'wxpay';
        }

        if(!empty($_REQUEST['orderNo'])&&!empty($_REQUEST['payid'])) $pay_code='jubaopay';
        $icon = 0;
        $pay_id = 0;

        if(empty($_REQUEST['code']) && !empty($order_sn) && $order_sn != 'ajax'){

            $is_paid = $this->db->getstr("SELECT is_paid FROM ###_pay_log WHERE log_id = '$order_sn'");
            $icon = 2;
            $is_success = $is_paid ? 1 : 0;
            $msg = $is_paid ? '支付成功' : '支付失败';
            $pay_id = $order_sn;

        }else{
            /* 参数是否为空 */
            if (empty($pay_code))
            {
                $msg = '支付方式不存在';
            }
            else
            {
                /* 检查code里面有没有问号 */
                if (strpos($pay_code, '?') !== false)
                {
                    $arr1 = explode('?', $pay_code);
                    $arr2 = explode('=', $arr1[1]);

                    $_REQUEST['code']   = $arr1[0];
                    $_REQUEST[$arr2[0]] = $arr2[1];
                    $_GET['code']       = $arr1[0];
                    $_GET[$arr2[0]]     = $arr2[1];
                    $pay_code           = $arr1[0];
                }

                /* 判断是否启用 */
                $payment = $this->db->get("SELECT * FROM ###_payment WHERE pay_code = '$pay_code' AND enabled = 1");
                if (empty($payment))
                {
                    $msg = '支付方式不可用';
                }
                else
                {
                    $plugin_file = AppDir.'includes/modules/payment/' . $pay_code . '.php';
                    /* 检查插件文件是否存在，如果存在则验证支付是否成功，否则则返回失败信息 */
                    if (is_file($plugin_file))
                    {
                        /* 根据支付方式代码创建支付类的对象并调用其响应操作方法 */
                        include_once($plugin_file);

                        $payment = new $pay_code();
                        if($order_sn == 'ajax'){
                            @$payment->respond();die;
                        }elseif(isset($_REQUEST['ajax'])){
                            if((@$payment->respond())){ echo 'ok';die; }
                            else{ echo 'error';die; }
                        }else{
                            if(@$payment->respond()){
                                $msg = '恭喜您，支付成功！';
                                $icon = 2;
                                if(isset($_SESSION['pay_id']) && $_SESSION['pay_id']){
                                    $pay_id = $_SESSION['pay_id'];
                                }
                            }else{
                                $msg = '支付失败';
                            }
                        }
                    }
                    else
                    {
                        $msg = '支付方式不存在';
                    }
                }
            }
        }
        if($_REQUEST['ajax']){
            die(json_encode(array('is_success'=>$is_success)));
        }

        //判断支付类型
        $order_type = 1;
        if($pay_id){
            $this->load->model('payment');
            $pay_log = $this->payment->pay_log($pay_id);
            $order_type = $pay_log['order_type'];
        }

        if($icon==2 && $order_type==2){
            $this->smarty->display('yunbuy/respond_success.html');die;
        }else{
            $this->msg($msg,array('iniframe'=>false,'url'=>url('/member/recchage'),'icon'=>$icon,array(
                array('text'=>'继续充值','link'=>'/member/recchage'),
                array('text'=>'返回首页','link'=>'/'),
            )));
        }
    }

    //微信支付扫码页
    function weixin_qrcode(){
        $data['url'] = urldecode($_POST['url']);
        $data['log_id'] = intval($_POST['log_id']);
        $data['money'] = trim($_POST['money']);
        $this->smarty->assign('data',$data);
        $this->display_before(array('title'=>'微信扫码支付'));
        $this->smarty->display('yunbuy/payment_weixin.html');
    }

    //生成微信二维扫码
    function build_qrcode(){
        $data = urldecode($_GET['data']);
        require AppDir.'library/phpqrcode/phpqrcode.php';
        QRcode::png($data, false, "L", 6);
        exit();
    }
	
    /**
     * 获取时时彩开奖结果
     * $qihao 格式如:141107006
     */
    function lottery($qihao=''){
        $this->load->model('yunbuy');
        echo $this->yunbuy->lottery_code($qihao);
    }
	
    /**
     * 总参与人数
     * 公益金计算
     */
    function bidCount(){
        #总参与
        $data = $this->base->read_static_cache('bidcount','');
        $data['count'] = str_pad($data['logCount'],9,'0',STR_PAD_LEFT);
		
        #公益金
        $gjj_js = $this->site_config['gjj_js'];
        $gjj_bl = $this->site_config['gjj_bl'];
		
        $count = number_format($gjj_js+$data['logCount']*$gjj_bl/100,2);
        $data['gjj'] = str_pad($count,9,'0',STR_PAD_LEFT);
		
        //*** 定期发送中奖消息队列 start
        $data = $this->base->read_static_cache('msg_queue','');
        //获取缓存后清除，防止重复发送
        $this->base->write_static_cache('msg_queue', array(), '');
        //发送消息
        if($data !== false && count($data) > 0){
            foreach($data as $k=>$v){
                $this->smarty->assign('username',$v['username']);
                $this->smarty->assign('goodsname',$v['goods_name']);

                if($v['type'] == 'wx'){ //发送微信推送
                    $this->load->model('wxapi');
                    $this->wxapi->sendTextMessage($v['openid'], $v['msg']);
                }
                elseif($v['type'] == 'sms'){ //发送中奖短信
                    $this->load->library('sms');
                    $ret = $this->sms->sendSmsTpl($v['mobile'], $v['tpl']);
                }
                elseif($v['type'] == 'app'){ //发送app推送
                    require_once(AppDir.'library/Jpush_send.class.php');
                    $fetion = new Jpush_send();
                    $res=$fetion->send_pub(array('alias'=>array($v['username'])), $v['msg'], array('winpage'=>'./html/yunbuy_detail','id'=>$v['buy_id']));
                }
                elseif($v['type'] == 'email'){ //发送中奖邮件
                    $this->load->library('mail');
                    $this->mail->sendMailTpl($v['email'], $v['tpl']);
                }
            }
        }
        //*** 定期发送中奖消息队列 end

        die(json_encode($data));
    }

    /**
     * 清除缓存
     */
    function clearCaches(){
        if(!$_POST) die;
        $type = trim($_POST['type']);

        $count = $this->base->clear_caches($type);
        die($count);
    }

    /** 管理员授权前台会员登录
     * @param $mid
     */
    function auth_login($mid){
        if(isset($_SESSION['gid']) && $_SESSION['gid']<2){
            $this->load->model('member');
            $user = $this->db->get("SELECT * FROM ###_member WHERE mid='".$mid."'");
            if(!$user['is_robots']){
                exit($this->msg('正常会员禁止授权登录！',array('iniframe'=>false,'url'=>'/')));
            }
            if($_SESSION['mid']){
                $this->member->logout();
            }
            $this->member->setLogin($user, "管理员授权前台会员登录");
            exit($this->msg('授权登录中...',array('iniframe'=>false,'icon'=>1,'url'=>'/member')));
        }else{
            $this->msg();
        }
    }

	function kf(){
		if(STPL == 'mobile'){
            $row['head'] = '关于信力';
            $this->smarty->assign('row',$row);
            $this->smarty->display('kf.html');
        }
	}

}