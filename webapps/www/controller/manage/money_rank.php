<?php

/**
 * Name 会员管理
 * Class member_adm
 */

class money_rank extends Lowxp{
    function __construct(){
        parent::__construct();
        #加载
        $this->load->model('member');
    }

    function index($page=1){
        #检索
        $conds = $this->getFilterCond();
        $condition = $conds['where'];

        #分页
        $this->load->model('page');
        $_GET['page'] = intval($page);
        $this->page->set_vars(array(
            'per' => (int)$this->common['page_listrows']
        ));
		$year=date("Y");
		$month=date("m");

        $sql = "SELECT DISTINCT main.*,a.username,a.user_money,a.db_scores,a.commission,a.comreg_points from (
		select *,'commission' rankcode,'返利' rankname from ###_commission_rank where rank<=10 and year<=$year and month<$month
union select *,'consume' rankcode,'消费' rankname from ###_consume_rank where rank<=10 and year<=$year and month<$month
union select *,'proxyconsume' rankcode,'代理消费' rankname from ###_proxyconsume_rank where rank<=5  and year<=$year and month<$month) main
inner join ###_member a on main.mid=a.mid 
".($condition!=''?" WHERE $condition":'')."
order by year desc,month desc,rankcode,rank";
        $list = $this->page->hashQuery($sql)->result_array();

        unset($_GET['page']);
        $this->smarty->assign($_GET);
        $this->smarty->assign('list',$list);
        $this->smarty->display('manage/member/money_rank.html');
        
    }
	function getFilterCond(){
        $where = ' 1 ';


        if(!empty($_GET['username'])){
            $where .= " AND username like '%".$_GET['username']."%'";
        }
        if(!empty($_GET['year'])){
            $where .= " AND main.year =".$_GET['year'];
        }
        if(!empty($_GET['month'])){
            $where .= " AND main.month =".$_GET['month'];
        }
        
        if(!empty($_GET['rankcode'])){
            $where .= " AND main.rankcode ='".$_GET['rankcode']."'";
        }
        if(!empty($_GET['status']) ||isset($_GET['status'])){
            $where .= " AND main.status =".$_GET['status'];
         }
       
        return array('where'=>$where);
    }
	function fund($mid){
		
		$rankcode=$_REQUEST["rankcode"];
		$year=$_REQUEST["year"];
		$month=$_REQUEST["month"];
		if($mid==0){
			$mid=$_REQUEST["mid"];
		}
        $this->smarty->assign('rankcode',$rankcode);
        $this->smarty->assign('year',$year);
        $this->smarty->assign('month',$month);
		if($rankcode=="commission"){
			die("返利排行榜奖励计划神秘礼物，另外可做数据参考作用！");
		}

        
		if($rankcode=="consume"){
			$top[1]=5000;
			$pre[1]=0.06;
			$top[2]=3000;
			$pre[2]=0.05;
			$top[3]=2000;
			$pre[3]=0.04;
			for($i=4;$i<=10;$i++){
			  $top[3]=1000;
			  $pre[3]=0.02;
			}
		}else{
			$pre[1]=0.015;
			$pre[2]=0.01;
			$pre[3]=0.005;
		}

        $row = $this->db->get("select main.*,a.username from ###_".$rankcode."_rank main
inner join ###_member a on main.mid=a.mid
where year=$year and month=$month and a.mid=$mid");
        $this->smarty->assign('row',$row);
		

		$rank=$row["rank"];
		if($rankcode=="consume"){
			$fund_description="每天更新当月用户消费前10的消费情况，可查询历史月份消费前5的相关信息。<br />
			用户港淘榜前三分别获得当月消费总额的6%，5%，4%到账户余额，最高封顶分别是5000，3000，2000，<br />
			第四到第十获得当月消费额总额的2%到账户余额里面最高限额1000，前提是必须月总消费满2000";
            $moneytypedec="奖励淘币";
		}else{
			$fund_description="每天更新代理当月代理用户总消费前3的情况。可查询历史月份代理总消费前3的相关信息。<br />
			代理光荣榜前三分别获得当月代理消费总额的1.5%，1%，0.5%到佣金账户里面";
            $moneytypedec="奖励佣金";
		}
   
		$this->smarty->assign("fund_description",$fund_description);
		$fund_money=0;
		$cost=$row["cost"];
		$rank=$row["rank"];
		if($rankcode=="consume"){
			if($cost>2000 || $cost==2000){
				$fund_money=$cost*$pre[$rank]+0;
				if($fund_money>$top[$rank]){
					$fund_money=$top[$rank];
				}
			}
		}else{
			$fund_money=$cost*$pre[$rank]+0;
		}
		
		$this->smarty->assign("fund_money",$fund_money);

		//提交
        if(isset($_POST['Submit'])){
            $post = $_POST['post'];
            $input = array();
            $input['mid'] = $mid;
			if($fund_money==0||$row["status"]==1){
                $this->tip("条件不符合",array('inIframe'=>true,'type'=>1));
				die;
			}
            if($rankcode=="consume"){
               $input['user_money'] = $fund_money;
               $input['desc'] = $year."年".$month."月，月消费榜排名第".$rank."名奖励淘币".$fund_money."元";
               $res = $this->member->accountlog($rankcode."fund",$input);
            }
            else{
               $input['commission'] = $fund_money;
               $input['desc'] = $year."年".$month."月，月代理榜排名第".$rank."名奖励佣金".$fund_money."元";
               $insert_arr = array();
               $insert_arr['mid'] =   $mid;
               $insert_arr['username'] = $row["username"];
               $insert_arr['ivt_username'] = '代理榜奖励';
               $insert_arr['order_id'] = '';
               $insert_arr['total'] = $cost;
               $insert_arr['commission'] = $fund_money;
               $insert_arr['desc'] =  $input['desc'];
               $this->member->save_commission($insert_arr);
            }

            admin_log('返利榜返利：' . $row['username']);

			$this->db->query("update zz_".$rankcode."_rank set status=1, description='".$moneytypedec.$fund_money."元' 
			where year=$year and month=$month and mid=$mid and status=0");
			
            if(isset($res['code']) && $res['code']==0){
                $this->tip($res['message'],array('inIframe'=>true));
                //$this->exeJs("parent.location.href='/manage#!category/index'");
                $this->exeJs("parent.com.xhide();parent.main.refresh()");
            }else{
                $this->tip($res['message'],array('inIframe'=>true,'type'=>1));
            }
            die;
        }

        $this->smarty->display('manage/member/money_rank_fund.html');
	}

}