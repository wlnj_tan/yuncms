rem  进入mysql的控制台后，使用source命令执行
rem Mysql>source 【sql脚本文件的路径全名】 或 Mysql>\. 【sql脚本文件的路径全名】，示例：
rem source d:\test\ss.sql 或者 \. d:\test\ss.sql
rem dir /B
rem xinli_dev_(.*)  rename xinli_dev_$1 $1
rem show variables like "%char%" set names utf8;
rem "C:\Program Files\MySQL\MySQL Server 5.7\bin\mysql" -uroot -proot -Dxinli_dev show variables like "%char%";
set mysqlcmd="C:\Program Files\MySQL\MySQL Server 5.7\bin\mysql" -uroot -proot -Dxinli_dev

%mysqlcmd%<zz_account_log.sql
%mysqlcmd%<zz_ad.sql
%mysqlcmd%<zz_app.sql
%mysqlcmd%<zz_app_log.sql
%mysqlcmd%<zz_article.sql
%mysqlcmd%<zz_auction_log.sql
%mysqlcmd%<zz_award_ivt.sql
%mysqlcmd%<zz_block.sql
%mysqlcmd%<zz_bonus.sql
%mysqlcmd%<zz_brand.sql
%mysqlcmd%<zz_business.sql
%mysqlcmd%<zz_cart.sql
%mysqlcmd%<zz_category.sql
%mysqlcmd%<zz_cod.sql
%mysqlcmd%<zz_comment.sql
%mysqlcmd%<zz_commission.sql
%mysqlcmd%<zz_commission_rank.sql
%mysqlcmd%<zz_config.sql
%mysqlcmd%<zz_config_other.sql
%mysqlcmd%<zz_consume_rank.sql
%mysqlcmd%<zz_diskfile.sql
%mysqlcmd%<zz_express.sql
%mysqlcmd%<zz_files.sql
%mysqlcmd%<zz_folder.sql
%mysqlcmd%<zz_gallery_images.sql
%mysqlcmd%<zz_gallery_tag.sql
%mysqlcmd%<zz_gbook.sql
%mysqlcmd%<zz_goods.sql
%mysqlcmd%<zz_goods_activity.sql
%mysqlcmd%<zz_goods_cat.sql
%mysqlcmd%<zz_goods_item.sql
%mysqlcmd%<zz_goods_order.sql
%mysqlcmd%<zz_goods_order_item.sql
%mysqlcmd%<zz_goods_order_log.sql
%mysqlcmd%<zz_goods_spec.sql
%mysqlcmd%<zz_goods_spec_item.sql
%mysqlcmd%<zz_images.sql
%mysqlcmd%<zz_images_thumb.sql
%mysqlcmd%<zz_lang.sql
%mysqlcmd%<zz_linkage.sql
%mysqlcmd%<zz_links.sql
%mysqlcmd%<zz_lottery.sql
%mysqlcmd%<zz_mail.sql
%mysqlcmd%<zz_member.sql
%mysqlcmd%<zz_member_account.sql
%mysqlcmd%<zz_member_address.sql
%mysqlcmd%<zz_member_assist.sql
%mysqlcmd%<zz_member_bankcard.sql
%mysqlcmd%<zz_member_bonus.sql
%mysqlcmd%<zz_member_comreg.sql
%mysqlcmd%<zz_member_fri.sql
%mysqlcmd%<zz_member_login_history.sql
%mysqlcmd%<zz_member_login_log.sql
%mysqlcmd%<zz_member_other.sql
%mysqlcmd%<zz_member_rank.sql
%mysqlcmd%<zz_member_visit.sql
%mysqlcmd%<zz_menus.sql
%mysqlcmd%<zz_mobilecat.sql
%mysqlcmd%<zz_module.sql
%mysqlcmd%<zz_module_field.sql
%mysqlcmd%<zz_msg.sql
%mysqlcmd%<zz_m_user.sql
%mysqlcmd%<zz_m_user_group.sql
%mysqlcmd%<zz_m_user_log.sql
%mysqlcmd%<zz_nav.sql
%mysqlcmd%<zz_oauth_wx.sql
%mysqlcmd%<zz_page.sql
%mysqlcmd%<zz_payment.sql
%mysqlcmd%<zz_pay_log.sql
%mysqlcmd%<zz_plate_log.sql
%mysqlcmd%<zz_posid.sql
%mysqlcmd%<zz_proxyconsume_rank.sql
%mysqlcmd%<zz_rec.sql
%mysqlcmd%<zz_search.sql
%mysqlcmd%<zz_send.sql
%mysqlcmd%<zz_share.sql
%mysqlcmd%<zz_sharecode.sql
%mysqlcmd%<zz_share_log.sql
%mysqlcmd%<zz_signin.sql
%mysqlcmd%<zz_sms.sql
%mysqlcmd%<zz_statistic_view_browser.sql
%mysqlcmd%<zz_statistic_view_city.sql
%mysqlcmd%<zz_statistic_view_info.sql
%mysqlcmd%<zz_statistic_view_log.sql
%mysqlcmd%<zz_statistic_view_platform.sql
%mysqlcmd%<zz_templates.sql
%mysqlcmd%<zz_usecode_log.sql
%mysqlcmd%<zz_verify_code.sql
%mysqlcmd%<zz_verify_idcard.sql
%mysqlcmd%<zz_virtual.sql
%mysqlcmd%<zz_voice.sql
%mysqlcmd%<zz_withdraw_commission.sql
%mysqlcmd%<zz_wx_autoreply_keyword.sql
%mysqlcmd%<zz_wx_autoreply_rule.sql
%mysqlcmd%<zz_wx_autoreply_text.sql
%mysqlcmd%<zz_wx_logs.sql
%mysqlcmd%<zz_wx_menu_data.sql
%mysqlcmd%<zz_wx_news.sql
%mysqlcmd%<zz_wx_news_item.sql
%mysqlcmd%<zz_wx_reply.sql
%mysqlcmd%<zz_wx_token.sql
%mysqlcmd%<zz_yunbuy.sql
%mysqlcmd%<zz_yuncart.sql
%mysqlcmd%<zz_yuncat.sql
%mysqlcmd%<zz_yundb.sql
%mysqlcmd%<zz_yunorder.sql
%mysqlcmd%<zz_yunwin.sql


