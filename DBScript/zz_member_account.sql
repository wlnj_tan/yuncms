-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_member_account`
--

DROP TABLE IF EXISTS `zz_member_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_member_account` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mid` int(10) NOT NULL COMMENT '会员ID',
  `username` varchar(255) NOT NULL COMMENT '会员',
  `admin_id` int(10) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `admin_user` varchar(255) NOT NULL DEFAULT '' COMMENT '管理员名称',
  `amount` decimal(10,2) NOT NULL COMMENT '充值金额',
  `fee` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `add_time` int(10) NOT NULL COMMENT '操作时间',
  `pay_id` int(10) NOT NULL DEFAULT '0' COMMENT '支付ID',
  `pay_name` varchar(60) NOT NULL DEFAULT '' COMMENT '支付名称',
  `pay_code` varchar(60) NOT NULL DEFAULT '' COMMENT '支付代码',
  `pay_time` int(10) NOT NULL DEFAULT '0' COMMENT '支付时间',
  `user_note` text COMMENT '用户备注',
  `admin_note` text COMMENT '管理员备注',
  `type` tinyint(1) NOT NULL COMMENT '类型:1充值2提现',
  `status` tinyint(1) NOT NULL COMMENT '支付状态:1待付款2已完成3已取消',
  `gotime` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_member_account`
--

LOCK TABLES `zz_member_account` WRITE;
/*!40000 ALTER TABLE `zz_member_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_member_account` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:20
