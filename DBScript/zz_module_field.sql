-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_module_field`
--

DROP TABLE IF EXISTS `zz_module_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_module_field` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `field` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(30) NOT NULL DEFAULT '',
  `tips` varchar(150) NOT NULL DEFAULT '',
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `minlength` int(10) unsigned NOT NULL DEFAULT '0',
  `maxlength` int(10) unsigned NOT NULL DEFAULT '0',
  `pattern` varchar(255) NOT NULL DEFAULT '',
  `errormsg` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(20) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL DEFAULT '',
  `setup` mediumtext NOT NULL,
  `ispost` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `unpostgroup` varchar(60) NOT NULL DEFAULT '',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_module_field`
--

LOCK TABLES `zz_module_field` WRITE;
/*!40000 ALTER TABLE `zz_module_field` DISABLE KEYS */;
INSERT INTO `zz_module_field` VALUES (14,2,'posid','推荐位','',0,0,0,'','','','posid','',1,'0',94,1,1),(1,1,'createtime','发布时间','',1,0,0,'','','','datetime','',0,'0',96,1,1),(8,2,'catid','栏目','',1,1,6,'','必须选择一个栏目','','catid','',1,'',0,1,1),(37,5,'intro','简介','',0,0,0,'','','','textarea','{\"fieldtype\":\"text\",\"width\":\"\",\"height\":\"\",\"default\":\"\"}',0,'',0,1,0),(2,1,'status','状态','',0,0,0,'','','','radio','{\"options\":\"\\u5df2\\u5ba1\\u6838|1\\r\\n\\u672a\\u5ba1\\u6838|0\",\"fieldtype\":\"tinyint\",\"numbertype\":\"1\",\"labelwidth\":\"75\",\"default\":\"1\"}',0,'0',100,1,1),(45,6,'images','广告图片','',0,0,0,'','','','images','{\"upload_maxnum\":\"10\",\"upload_maxsize\":\"\",\"upload_allowext\":\"*.gif;*.jpg;*.jpeg;*.png\",\"watermark\":\"0\",\"more\":\"0\"}',0,'',1,1,0),(46,7,'createtime','发布时间','',1,0,0,'','','','datetime','',0,'0',100,1,1),(9,2,'title','标题','',1,1,80,'','标题必填3-80个字','','title','{\"thumb\":\"1\",\"btntext\":\"\",\"style\":\"1\",\"size\":\"\"}',1,'0',0,1,1),(53,2,'publish','来源','',0,0,0,'','','','text','{\"size\":\"\",\"default\":\"\",\"ispassword\":\"0\"}',0,'',0,1,0),(15,2,'createtime','发布时间','',0,0,0,'','','','datetime','',1,'0',96,1,1),(42,6,'title','广告位名称','',1,0,0,'','','','title','{\"thumb\":\"1\",\"btntext\":\"\\u5e7f\\u544a\\u4f4d\\u622a\\u56fe\",\"style\":\"0\",\"size\":\"\"}',0,'',0,1,0),(16,2,'template','模板','',0,0,0,'','','','template','',0,'0',99,1,1),(39,4,'content','内容','',1,0,0,'','','','textarea','{\"fieldtype\":\"text\",\"width\":\"\",\"height\":\"\",\"default\":\"\"}',0,'',10,1,0),(13,2,'hits','点击次数','',0,0,8,'','','','number','{\"size\":\"60\",\"numbertype\":\"1\",\"decimaldigits\":\"0\",\"default\":\"0\"}',1,'0',95,1,1),(44,6,'height','广告位高度','广告位高度请输入数字类型',0,0,0,'digits','','','text','{\"size\":\"\",\"default\":\"\",\"ispassword\":\"0\"}',0,'',0,1,0),(17,2,'status','状态','',0,0,0,'','','','radio','{\"options\":\"\\u5f00\\u542f|1\\r\\n\\u5173\\u95ed|0\",\"fieldtype\":\"tinyint\",\"numbertype\":\"1\",\"labelwidth\":\"75\",\"default\":\"1\"}',0,'0',100,1,1),(10,2,'keywords','SEO关键词','',0,0,80,'','','','text','{\"size\":\"\",\"default\":\"\",\"ispassword\":\"0\",\"fieldtype\":\"varchar\"}',1,'0',97,1,1),(12,2,'content','内容','',0,0,0,'','','','editor','{\"edittype\":\"kindeditor\",\"toolbar\":\"full\",\"default\":\"\",\"height\":\"\"}',1,'0',0,1,1),(57,4,'email','邮箱','',1,0,0,'','','','text','{\"size\":\"\",\"default\":\"\",\"ispassword\":\"0\"}',0,'',4,1,0),(4,1,'content','内容','',0,0,0,'','','','editor','{\"edittype\":\"kindeditor\",\"toolbar\":\"basic\",\"default\":\"\",\"height\":\"\"}',0,'',0,1,1),(34,5,'title','链接名称','',1,0,0,'','','','title','{\"thumb\":\"1\",\"style\":\"0\",\"size\":\"\"}',0,'',0,1,0),(11,2,'description','SEO描述','',0,0,0,'','','','textarea','{\"fieldtype\":\"mediumtext\",\"width\":\"\",\"height\":\"\",\"default\":\"\"}',1,'0',98,1,1),(28,4,'createtime','发布时间','',0,0,0,'','','','datetime','',0,'0',96,1,1),(35,5,'siteurl','链接地址','',0,0,0,'url','','','text','{\"size\":\"\",\"default\":\"http:\\/\\/\",\"ispassword\":\"0\"}',0,'',0,1,0),(50,7,'linkurl','链接地址','',0,0,0,'','','','text','{\"size\":\"\",\"default\":\"\",\"ispassword\":\"0\"}',0,'',0,1,0),(62,4,'ip','ip','',0,0,0,'','','','text','{\"size\":\"\",\"default\":\"\",\"ispassword\":\"0\"}',0,'',0,1,0),(49,7,'title','导航名称','',1,0,0,'','','','title','{\"thumb\":\"0\",\"btntext\":\"\",\"style\":\"0\",\"size\":\"300\"}',0,'',0,1,0),(47,7,'status','状态','',0,0,0,'','','','radio','{\"options\":\"\\u663e\\u793a|1\\r\\n\\u9690\\u85cf|0\",\"fieldtype\":\"tinyint\",\"numbertype\":\"1\",\"labelwidth\":\"\",\"default\":\"1\"}',0,'0',96,1,1),(31,5,'createtime','发布时间','',1,0,0,'','','','datetime','',0,'0',96,1,1),(51,7,'isblank','新窗口','',0,0,0,'','','','radio','{\"options\":\"\\u662f|1\\r\\n\\u5426|0\",\"fieldtype\":\"varchar\",\"numbertype\":\"1\",\"labelwidth\":\"\",\"default\":\"0\"}',0,'',0,1,0),(3,1,'title','标题','',1,0,0,'','','','title','{\"thumb\":\"0\",\"style\":\"0\",\"size\":\"\"}',0,'',0,1,1),(7,1,'hits','点击次数','',0,0,0,'','','','number','{\"size\":\"60\",\"numbertype\":\"1\",\"decimaldigits\":\"0\",\"default\":\"0\"}',0,'',95,1,1),(40,6,'createtime','发布时间','',1,0,0,'','','','datetime','',0,'0',96,1,1),(59,4,'name','姓名','',1,0,0,'','','','text','{\"size\":\"\",\"default\":\"\",\"ispassword\":\"0\"}',0,'',0,1,0),(48,7,'type','显示位置','',1,0,0,'','','','radio','{\"options\":\"\\u9876\\u90e8|1\\r\\n\\u5e95\\u90e8|2\\r\\n\\u4e3b\\u5bfc\\u822a|3\\r\\n\\u5de6\\u5bfc\\u822a|4\",\"fieldtype\":\"varchar\",\"numbertype\":\"1\",\"labelwidth\":\"\",\"default\":\"1\"}',0,'',0,1,0),(33,5,'linktype','链接类型','',1,0,0,'','','','radio','{\"options\":\"\\u5e95\\u90e8\\u6587\\u5b57\\u94fe\\u63a5|1\\r\\n\\u5e95\\u90e8\\u56fe\\u7247\\u94fe\\u63a5|2\",\"fieldtype\":\"varchar\",\"numbertype\":\"1\",\"labelwidth\":\"\",\"default\":\"1\"}',0,'',0,1,0),(41,6,'status','状态','',0,0,0,'','','','radio','{\"options\":\"\\u5df2\\u5ba1\\u6838|1\\r\\n\\u672a\\u5ba1\\u6838|0\",\"fieldtype\":\"tinyint\",\"numbertype\":\"1\",\"labelwidth\":\"75\",\"default\":\"1\"}',0,'0',100,1,1),(29,4,'status','状态','',0,0,0,'','','','radio','{\"options\":\"\\u5df2\\u5ba1\\u6838|1\\r\\n\\u672a\\u5ba1\\u6838|0\\r\\n\",\"fieldtype\":\"tinyint\",\"numbertype\":\"1\",\"labelwidth\":\"75\",\"default\":\"0\"}',0,'0',100,1,1),(43,6,'width','广告位宽度','广告位宽度请输入数字类型',0,0,0,'digits','','','text','{\"size\":\"\",\"default\":\"\",\"ispassword\":\"0\"}',0,'',0,1,0),(32,5,'status','状态','',0,0,0,'','','','radio','{\"options\":\"\\u5df2\\u5ba1\\u6838|1\\r\\n\\u672a\\u5ba1\\u6838|0\",\"fieldtype\":\"tinyint\",\"numbertype\":\"1\",\"labelwidth\":\"75\",\"default\":\"1\"}',0,'0',100,1,1),(60,2,'link','外部链接','',0,0,0,'','','','text','{\"size\":\"\",\"default\":\"\",\"ispassword\":\"0\"}',0,'',0,1,0),(61,7,'rec','hot','',0,0,0,'','','','radio','{\"options\":\"\\u662f|1\\r\\n\\u5426|0\",\"fieldtype\":\"tinyint\",\"numbertype\":\"1\",\"labelwidth\":\"\",\"default\":\"0\"}',0,'',0,1,0),(63,6,'bgcolor','背景颜色','',0,0,0,'','','','text','{\"size\":\"\",\"default\":\"\",\"ispassword\":\"0\"}',0,'',0,1,0),(64,8,'createtime','发布时间','',1,0,0,'','','','datetime','',0,'0',96,1,1),(65,8,'status','状态','',0,0,0,'','','','radio','{\"options\":\"\\u5df2\\u5ba1\\u6838|1\\r\\n\\u672a\\u5ba1\\u6838|0\",\"fieldtype\":\"tinyint\",\"numbertype\":\"1\",\"labelwidth\":\"75\",\"default\":\"1\"}',0,'0',100,1,1),(66,8,'title','标题','',0,0,0,'','','','text','{\"size\":\"\",\"default\":\"\",\"ispassword\":\"0\"}',0,'',0,1,0),(67,8,'thumb','图片','',0,0,0,'','','','images','{\"upload_maxnum\":\"\",\"upload_maxsize\":\"\",\"upload_allowext\":\"*.gif;*.jpg;*.jpeg;*.png\",\"watermark\":\"0\",\"more\":\"0\"}',0,'',0,1,0),(68,8,'type','推荐位','',1,0,0,'','','','select','{\"options\":\"\\u9996\\u9875\\u7126\\u70b9\\u56fe\\u53f3\\u4fa7\\u593a\\u5b9d\\u63a8\\u8350\\u4f4d|1\\r\\n\\u9996\\u9875\\u7126\\u70b9\\u56fe\\u4e0b\\u65b9\\u7ade\\u62cd\\u63a8\\u8350\\u4f4d|2\",\"multiple\":\"0\",\"fieldtype\":\"varchar\",\"numbertype\":\"1\",\"size\":\"\",\"default\":\"\"}',0,'',0,1,0),(69,8,'recid','推荐内容ID','',1,0,0,'number','','','text','{\"size\":\"\",\"default\":\"\",\"ispassword\":\"0\"}',0,'',0,1,0);
/*!40000 ALTER TABLE `zz_module_field` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:18
