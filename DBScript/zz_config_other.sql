-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_config_other`
--

DROP TABLE IF EXISTS `zz_config_other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_config_other` (
  `varname` varchar(20) NOT NULL DEFAULT '' COMMENT '参数名',
  `value` text NOT NULL COMMENT '参数值',
  `lang` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '多语言',
  KEY `varname` (`varname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_config_other`
--

LOCK TABLES `zz_config_other` WRITE;
/*!40000 ALTER TABLE `zz_config_other` DISABLE KEYS */;
INSERT INTO `zz_config_other` VALUES ('plate_db_points','50',0),('plate_get_points_2','20',0),('plate_points_1','1',0),('plate_get_type_1','1',0),('plate_get_points_1','50',0),('plate_pay_points','20',0),('plate_get_type_2','1',0),('plate_points_2','2',0),('plate_get_points_3','20',0),('plate_get_type_3','2',0),('plate_points_3','3',0),('plate_get_points_4','10',0),('plate_get_type_4','2',0),('plate_points_4','4',0),('plate_get_points_5','5',0),('plate_get_type_5','2',0),('plate_points_5','10',0),('plate_points_0','100',0),('plate_start_time','2016-05-11 13:30:53',0),('plate_end_time','2020-05-12 13:30:57',0);
/*!40000 ALTER TABLE `zz_config_other` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:15
