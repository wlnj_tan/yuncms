-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_menus`
--

DROP TABLE IF EXISTS `zz_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `parentid` smallint(5) DEFAULT NULL,
  `mod` varchar(30) DEFAULT NULL,
  `action` varchar(30) DEFAULT NULL,
  `param` varchar(100) DEFAULT NULL,
  `icon` varchar(10) DEFAULT NULL,
  `listorder` int(11) DEFAULT '0',
  `type` tinyint(2) DEFAULT '1' COMMENT '1:用于显示,2用于处理子菜单关系',
  `status` tinyint(1) DEFAULT '1',
  `issystem` tinyint(1) DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=174 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_menus`
--

LOCK TABLES `zz_menus` WRITE;
/*!40000 ALTER TABLE `zz_menus` DISABLE KEYS */;
INSERT INTO `zz_menus` VALUES (1,'系统',0,'setting','index','','',0,1,1,1,NULL),(2,'站点配置',1,'setting','index','','&#xe634;',0,1,1,NULL,NULL),(3,'文章',0,'website','index','','&#xe631;',3,1,1,1,NULL),(121,'添加/编辑一元港淘',120,'yunbuy','edit','','',0,2,0,0,NULL),(77,'文章碎片',89,'block','index','','&#xe619;',1,1,1,0,NULL),(78,'添加/编辑碎片',77,'block','edit','','',0,2,1,0,NULL),(79,'自定义导航',89,'content','index','nav','&#xe601;',1,1,1,0,'nav'),(15,'添加/编辑菜单',18,'menus','edit','',NULL,1,2,1,NULL,NULL),(31,'栏目管理',3,'category','index','','&#xe631;',0,1,1,0,NULL),(17,'扩展',0,'extend','extend','',NULL,6,1,1,1,NULL),(18,'后台菜单',1,'menus','index','','&#xe631;',1,1,1,NULL,NULL),(27,'模型管理',3,'module','index','','&#xe62d;',2,1,1,NULL,NULL),(22,'修改密码',2,'setting','chpass','','',0,2,1,NULL,NULL),(157,'删除内容',65,'content','del','article','',0,2,1,0,NULL),(28,'添加/编辑模型',27,'module','edit','','',1,2,1,NULL,NULL),(29,'字段管理',27,'field','index','','',2,2,1,0,NULL),(30,'添加/编辑字段',27,'field','edit','','',3,2,1,0,NULL),(32,'添加/编辑栏目',31,'category','edit','','',0,2,1,0,NULL),(70,'添加/编辑内容',69,'content','edit','gbook',NULL,0,2,1,0,'gbook'),(71,'修改内容',31,'content','edit','page','',0,2,1,0,NULL),(47,'推荐位',89,'posid','index','','&#xe632;',3,1,0,0,NULL),(48,'添加/编辑推荐位',47,'posid','edit','','',0,2,1,0,NULL),(65,'文章模型',3,'content','index','article','&#xe601;',0,1,1,0,'article'),(66,'添加/编辑内容',65,'content','edit','article',NULL,0,2,1,0,'article'),(98,'商品管理',97,'goods','index','','&#xe63c;',0,1,1,0,NULL),(97,'产品中心',0,'mall','index','','',1,1,1,1,NULL),(69,'在线留言',3,'content','index','gbook','&#xe601;',0,1,1,0,'gbook'),(72,'友情链接',89,'content','index','links','&#xe646;',0,1,1,0,'links'),(73,'添加/编辑内容',72,'content','edit','links',NULL,0,2,1,0,'links'),(74,'广告模型',89,'content','index','ad','&#xe601;',2,1,1,0,'ad'),(75,'添加/编辑内容',74,'content','edit','ad',NULL,0,2,1,0,'ad'),(76,'分配权限',2,'setting','node','1','',0,2,1,0,NULL),(81,'媒体库',17,'upload','media','','&#xe602;',1000,1,1,0,NULL),(87,'联动菜单',89,'linkage','index','','&#xe631;',4,1,1,0,NULL),(88,'添加/编辑联动菜单',87,'linkage','edit','','',0,2,1,0,NULL),(89,'模块',0,'content','index','','',5,1,1,1,NULL),(90,'微信',0,'wechat','','','&#xe615;',7,1,1,1,NULL),(91,'菜单设置',90,'wxmenu','index','','&#xe605;',0,1,1,1,NULL),(92,'素材管理',90,'wxmedia','','','&#xe605;',0,1,1,1,NULL),(93,'自动回复',90,'wxreply','','','&#xe605;',0,1,1,1,NULL),(95,'会员',0,'member','','','',2,1,0,1,NULL),(96,'会员管理',95,'member','index','','&#xe621;',0,1,1,0,NULL),(99,'账户明细',96,'member','account_log','','',0,2,1,0,NULL),(100,'添加商品',98,'goods','add','','',0,2,1,0,NULL),(102,'编辑商品',98,'goods','edit','','',0,2,1,0,NULL),(106,'订单管理',97,'order','index','','&#xe647;',10,1,0,0,NULL),(172,'弃奖管理',97,'yunbuy','order','money','&#xe647;',10,1,0,0,NULL),(107,'订单详情',106,'order','detail','','',0,2,0,0,NULL),(116,'充值提现',95,'member','member_account','','&#xe605;',2,1,1,0,NULL),(117,'资金入帐',95,'member','member_cash','','&#xe605;',2,1,1,0,NULL),(120,'港淘管理',97,'yunbuy','index','','&#xe605;',5,1,0,0,NULL),(123,'支付方式',1,'payment','index','','&#xe614;',2,1,0,0,NULL),(124,'添加/编辑',116,'member','member_account_edit','','',0,2,1,0,NULL),(125,'管理员列表',1,'admin','index','','&#xe614;',4,1,1,0,NULL),(126,'管理员角色',1,'admin','role','','&#xe614;',5,1,1,0,NULL),(127,'日志管理',1,'log','index','','&#xe614;',6,1,1,0,NULL),(128,'添加/编辑',125,'admin','edit','','',0,2,1,0,NULL),(129,'添加/编辑',126,'admin','edit_role','','',0,2,1,0,NULL),(130,'短信验证码日志',127,'log','sms','','',0,2,1,0,NULL),(158,'快递公司',1,'express','index','','&#xe614;',2,1,0,0,NULL),(132,'短信模板',1,'templates','index','','&#xe614;',3,1,0,0,NULL),(133,'控制面板',1,'timing','index','','&#xe641;',-1,1,1,0,NULL),(136,'港淘分类',97,'yuncat','index','','&#xe605;',6,1,0,0,NULL),(137,'港淘订单',97,'yunbuy','order','','&#xe605;',7,1,0,0,NULL),(138,'晒单管理',97,'share','index','','&#xe605;',8,1,0,0,NULL),(139,'实名认证',95,'member','verify_idcard','','&#xe63f;',1,1,1,0,NULL),(140,'会员留言',95,'member','message','','&#xe645;',4,1,1,0,NULL),(141,'账户明细',95,'member','account_log','','&#xe605;',5,1,1,0,NULL),(145,'会员等级',95,'member_rank','index','','&#xe640;',3,1,1,0,NULL),(171,'代理等级',95,'comregistry_rank','index','','&#xe640;',4,1,1,0,NULL),(173,'港淘榜奖励',95,'money_rank','index','','&#xe640;',4,1,1,0,NULL),(146,'邀请奖励',95,'member','award_ivt','','&#xe605;',6,1,1,0,NULL),(147,'佣金明细',95,'member','commission','','&#xe605;',7,1,1,0,NULL),(149,'佣金提现',95,'member','withdraw_commission','','&#xe605;',8,1,1,0,NULL),(167,'移动端分类',97,'mobilecat','index','','',6,1,1,0,NULL),(153,'商品品牌',97,'brand','index','','&#xe605;',1,1,1,0,NULL),(155,'抵用券',95,'bonus','index','','&#xe65a;',10,1,1,0,NULL),(156,'发送队列 ',132,'templates','send','','',0,2,0,0,NULL),(160,'商品分类',97,'goodscat','index','','&#xe605;',2,1,1,0,NULL),(170,'多媒体分类',17,'gallery_tag','index','','&#xe631;',0,1,1,0,'');
/*!40000 ALTER TABLE `zz_menus` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:26
