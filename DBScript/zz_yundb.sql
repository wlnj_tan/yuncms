-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_yundb`
--

DROP TABLE IF EXISTS `zz_yundb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_yundb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL COMMENT '订单编号',
  `mid` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `buy_id` int(11) NOT NULL COMMENT '港淘编号',
  `goods_name` varchar(60) NOT NULL COMMENT '商品名',
  `goods_price` decimal(20,0) NOT NULL COMMENT '商品总价',
  `price` decimal(20,0) NOT NULL COMMENT '单次价格',
  `qty` smallint(6) NOT NULL COMMENT '参与人次',
  `total` decimal(10,0) NOT NULL COMMENT '总价',
  `cover` int(11) NOT NULL COMMENT '封面',
  `qishu` smallint(6) NOT NULL COMMENT '期数',
  `multi` smallint(6) NOT NULL DEFAULT '1' COMMENT '参与多期',
  `auto_multi` smallint(3) NOT NULL DEFAULT '0' COMMENT '系统自动多期购买1是0否',
  `yun_code` longtext,
  `luck_code` char(50) DEFAULT NULL COMMENT '中奖号',
  `db_time` char(50) NOT NULL DEFAULT '' COMMENT '港淘时间',
  `timenum` varchar(50) DEFAULT NULL COMMENT '港淘时间值',
  `status` smallint(3) NOT NULL DEFAULT '1' COMMENT '状态:1未支付2已支付3已中奖4待揭晓5未中奖',
  `ip` varchar(30) NOT NULL DEFAULT '',
  `is_show` smallint(3) NOT NULL DEFAULT '1',
  `is_award` smallint(3) NOT NULL DEFAULT '0' COMMENT '是否领奖0未领奖1已领奖10待补偿11已补偿',
  `add_time` int(10) NOT NULL,
  `type` smallint(3) DEFAULT '1' COMMENT '类型',
  `sharecode` char(5) NOT NULL DEFAULT '' COMMENT '分享码',
  `fdis` smallint(3) DEFAULT '0' COMMENT '1永不失效',
  `agents` varchar(100) DEFAULT NULL COMMENT '访问终端',
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`,`order_id`,`buy_id`),
  KEY `tmid` (`mid`),
  KEY `buy_id` (`buy_id`),
  KEY `IDX_DB_TIME` (`db_time`),
  KEY `IDX_ORDER_ID` (`order_id`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_STATUS_BUY_ID` (`status`,`buy_id`),
  KEY `IDX_STATUS_DB_TIME` (`status`,`db_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_yundb`
--

LOCK TABLES `zz_yundb` WRITE;
/*!40000 ALTER TABLE `zz_yundb` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_yundb` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:24
