-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_member_bonus`
--

DROP TABLE IF EXISTS `zz_member_bonus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_member_bonus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bonus_id` int(11) NOT NULL DEFAULT '0' COMMENT '抵用券ID',
  `bonus_sn` bigint(20) NOT NULL DEFAULT '0' COMMENT '抵用券条码',
  `money` decimal(10,0) NOT NULL DEFAULT '0',
  `money_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0淘币 1余额',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '有效期',
  `mid` int(11) NOT NULL DEFAULT '0' COMMENT '所属会员ID',
  `used_time` int(11) NOT NULL DEFAULT '0' COMMENT '消费时间',
  `order_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '消费的订单ID',
  `desc` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_member_bonus`
--

LOCK TABLES `zz_member_bonus` WRITE;
/*!40000 ALTER TABLE `zz_member_bonus` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_member_bonus` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:14
