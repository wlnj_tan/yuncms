-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_payment`
--

DROP TABLE IF EXISTS `zz_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_payment` (
  `pay_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `pay_code` varchar(20) NOT NULL DEFAULT '',
  `pay_name` varchar(120) NOT NULL DEFAULT '',
  `pay_fee` varchar(10) NOT NULL DEFAULT '0',
  `pay_desc` text NOT NULL,
  `pay_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `pay_config` text NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_cod` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_online` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`pay_id`),
  UNIQUE KEY `pay_code` (`pay_code`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_payment`
--

LOCK TABLES `zz_payment` WRITE;
/*!40000 ALTER TABLE `zz_payment` DISABLE KEYS */;
INSERT INTO `zz_payment` VALUES (16,'jdpay','京东支付','0','京东支付',0,'a:2:{i:0;a:3:{s:4:\"name\";s:6:\"app_id\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:12:\"110241024003\";}i:1;a:3:{s:4:\"name\";s:7:\"app_key\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:32:\"l/ET2ZKFyG1nNJ7akUCX/e+XrmIsbdMf\";}}',1,0,1,''),(1,'alipay','支付宝','0','国内先进的网上支付平台。三种支付接口：担保交易，即时到账，双接口。在线即可开通，零预付，免年费，单笔阶梯费率，无流量限制。',2,'a:4:{i:0;a:3:{s:4:\"name\";s:14:\"alipay_account\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:22:\"chongwu@helpfooter.com\";}i:1;a:3:{s:4:\"name\";s:10:\"alipay_key\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:32:\"tjxvkaihmx07qvk1zqszjypi26l7ukb2\";}i:2;a:3:{s:4:\"name\";s:14:\"alipay_partner\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:16:\"2088221664536214\";}i:3;a:3:{s:4:\"name\";s:17:\"alipay_pay_method\";s:4:\"type\";s:6:\"select\";s:5:\"value\";s:1:\"2\";}}',1,0,1,''),(17,'alipayapp','支付宝APP','0','APP支付',0,'a:2:{i:0;a:3:{s:4:\"name\";s:17:\"alipayapp_account\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:22:\"chongwu@helpfooter.com\";}i:1;a:3:{s:4:\"name\";s:17:\"alipayapp_partner\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:16:\"2088221664536214\";}}',1,0,1,''),(8,'wxpay','微信端支付','0','微信支付',5,'a:4:{i:0;a:3:{s:4:\"name\";s:12:\"wxpay_app_id\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:18:\"wxed38866af330cfd8\";}i:1;a:3:{s:4:\"name\";s:16:\"wxpay_app_secret\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:32:\"31d0b2c9740ddd3c838f19b42de80887\";}i:2;a:3:{s:4:\"name\";s:11:\"wxpay_mchid\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:10:\"1333862901\";}i:3;a:3:{s:4:\"name\";s:9:\"wxpay_key\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:32:\"03941b924d12454219648d61a7b025e1\";}}',1,0,1,''),(13,'wxpayapp','微信APP支付','0','APP支付接口 <a target=\"_blank\" href=\"https://open.weixin.qq.com/cgi-bin/frame?t=home/app_tmpl&lang=zh_CN\">立即申请</a>',0,'a:4:{i:0;a:3:{s:4:\"name\";s:12:\"wxpay_app_id\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:18:\"wxb92981b6615910c7\";}i:1;a:3:{s:4:\"name\";s:16:\"wxpay_app_secret\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:32:\"a0df43b1bc855b161d7a45cfaa38eb31\";}i:2;a:3:{s:4:\"name\";s:11:\"wxpay_mchid\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:10:\"1400721802\";}i:3;a:3:{s:4:\"name\";s:9:\"wxpay_key\";s:4:\"type\";s:4:\"text\";s:5:\"value\";s:32:\"03941b924d12454219648d61a7b025e1\";}}',1,0,1,'');
/*!40000 ALTER TABLE `zz_payment` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:26
