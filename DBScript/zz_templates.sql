-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_templates`
--

DROP TABLE IF EXISTS `zz_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_templates` (
  `template_id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `template_code` varchar(30) NOT NULL DEFAULT '',
  `template_subject` varchar(200) NOT NULL DEFAULT '',
  `template_content` text NOT NULL,
  `last_modify` int(10) unsigned NOT NULL DEFAULT '0',
  `last_send` int(10) unsigned NOT NULL DEFAULT '0',
  `send_number` int(10) DEFAULT '0' COMMENT '发送次数',
  `type` varchar(10) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `is_system` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1系统模板，不可删除',
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `template_code` (`template_code`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_templates`
--

LOCK TABLES `zz_templates` WRITE;
/*!40000 ALTER TABLE `zz_templates` DISABLE KEYS */;
INSERT INTO `zz_templates` VALUES (1,'send_password','密码找回','{$user_name}您好！<br><br>\r\n\r\n您已经进行了密码重置的操作，请点击以下链接(或者复制到您的浏览器):<br>\r\n<a href=\"{$reset_email}\" target=\"_blank\">{$reset_email}</a><br><br>\r\n\r\n以确认您的新密码重置操作！<br><br>\r\n\r\n{$shop_name}<br>\r\n{$send_date}',1453878710,1425909372,0,'mail',1,1),(8,'register_validate','邮件验证','{$user_name}您好！<br><br>\r\n\r\n这封邮件是 {$shop_name} 发送的。你收到这封邮件是为了验证你注册邮件地址是否有效。如果您已经通过验证了，请忽略这封邮件。<br>\r\n请点击以下链接(或者复制到您的浏览器)来验证你的邮件地址:<br>\r\n<a href=\"{$validate_email}\" target=\"_blank\">{$validate_email}</a><br><br>\r\n\r\n{$site_config.site_name}<br>\r\n{$send_date}',1453878708,1453702244,0,'mail',1,1),(15,'sms_register','注册验证码','114521|verify_code',1473498528,1476753946,16,'sms',1,1),(17,'sms_cod','中奖短信','114951|username',1473147258,1477381984,105,'sms',1,1),(18,'sms_chpaypass','修改支付密码验证','114953|verify_code',1473147542,1477277168,12,'sms',1,1),(19,'mail_cod','中奖通知','恭喜获奖！{$username}，获奖商品：{$goodsname}；请登陆我们的官网领取！领取后3-7个工作日内发货！有任何问题可致电{$site_config.tel}咨询客服！欢迎你来，拍你所想，拍你所爱。',1453878705,1426493645,0,'mail',1,1),(20,'sms_bankcard','绑定银行卡验证','114953|verify_code',1473147535,1475539273,1,'sms',1,1),(27,'sms_password','密码找回','114523|verify_code',1473147077,1475806562,12,'sms',1,1),(26,'sms_verifymobile','绑定手机号验证','114953|verify_code',1473147363,1477303204,22,'sms',1,1),(28,'sms_manage','后台登陆验证','114958|verify_code',1473147479,0,0,'sms',1,1);
/*!40000 ALTER TABLE `zz_templates` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:25
