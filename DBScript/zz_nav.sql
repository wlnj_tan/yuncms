-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_nav`
--

DROP TABLE IF EXISTS `zz_nav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_nav` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `userid` int(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `url` varchar(60) NOT NULL DEFAULT '',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `lang` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL DEFAULT '1',
  `title` varchar(255) NOT NULL DEFAULT '',
  `title_style` varchar(40) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `linkurl` text NOT NULL,
  `isblank` varchar(255) NOT NULL DEFAULT '0',
  `rec` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_nav`
--

LOCK TABLES `zz_nav` WRITE;
/*!40000 ALTER TABLE `zz_nav` DISABLE KEYS */;
INSERT INTO `zz_nav` VALUES (1,1,1,'lnest_admin','/content/show//1',10,1405073435,1479870472,1,'3','关于信力','','','/content/index/2','0',0),(2,0,1,'lnest_admin','/content/show//2',2,1405303686,1417049890,1,'2','加入我们','','','/content/index/3','0',0),(3,1,1,'lnest_admin','/content/show//3',3,1405303698,1418646047,1,'2','服务协议','','','/content/index/42','0',0),(5,1,1,'lnest_admin','/content/show//5',5,1405303836,1447486111,1,'2','一元港淘规则','','','/content/index/71','0',0),(6,1,1,'lnest_admin','/content/show//6',6,1405303845,1418645074,1,'2','联系我们','','','/content/index/77','0',0),(8,1,1,'lnest_admin','/content/show//8',1,1450341752,1450485547,1,'2','首页','','','/','1',0),(30,1,1,'lnest_admin','/content/show//30',1,1431486891,1451551901,1,'3','首页','','','/','0',0),(41,0,2,'admin','/content/show//41',3,1450836978,0,1,'3','最新揭晓','','','/content/win','0',0),(20,0,1,'lnest_admin','/content/show//20',1,1416999155,1418644894,1,'1','联系客服','','','/content/index/77','0',0),(46,0,45,'whl15377993193','/content/show//46',9,1458183952,0,1,'3','邀请','','','/member/myivt','0',0),(22,0,1,'lnest_admin','/content/show//22',6,1417051849,1450837033,1,'3','晒单分享','','','/content/share','0',0),(28,0,1,'lnest_admin','/content/show//28',2,1420704500,1450352665,1,'2','最新揭晓','','','/content/win','0',0),(48,0,43,'tim','/content/show//48',7,1458728106,1458728959,1,'3','竞拍','','','/auction/lists','0',0),(42,0,2,'admin','/content/show//42',4,1451879699,1451890693,1,'3','十元区','','','/yunbuy?zq=10','0',0),(35,0,2,'admin','/content/show//35',5,1449196570,1458008111,1,'3','限购专区','','','/yunbuy?zq=limit','0',0),(44,0,23,'tim','/content/show//44',3,1453365953,1463104919,1,'3','百元区','','','/yunbuy?zq=100','0',0),(40,0,2,'admin','/content/show//40',0,1450520400,1451540533,1,'4','一元港淘','','','yunbuy','0',0),(36,1,2,'admin','/content/show//36',4,1450352698,0,1,'2','购物车','','','','0',0),(37,1,2,'admin','/content/show//37',5,1450352759,0,1,'2','我的港淘','','','','0',0),(50,0,46,'suhafe','/content/show//50',11,1463104860,1463121438,1,'3','大转盘','','','/plate','0',1),(51,0,2,'admin','/content/show//51',3,1449196570,1458008111,1,'3','港淘榜','','','/rank','0',0),(52,0,2,'admin','/content/show//51',7,1449196570,1458008111,1,'3','港淘圈','','','/yunbuy?zq=gtq','0',0),(53,1,2,'admin','/content/show//53',11,1449196570,1480171873,1,'3','联系我们','','','/content/index/77','0',0),(54,1,2,'admin','/content/show//54',9,1449196570,1480161903,1,'3','新闻资讯','','','/content/index/65','0',0),(55,0,2,'admin','/content/show//51',10,1449196570,1458008111,1,'3','购物车','','','/yunbuy/cart','0',0),(56,1,1,'lnest_admin','/content/show//56',12,1416999155,1418644894,1,'1','我的1元港淘','','','/member/db','0',0),(57,1,1,'lnest_admin','/content/show//57',13,1416999155,1418644894,1,'1','充值','','','/member/recchage#m','0',0),(58,1,1,'lnest_admin','/content/show//58',14,1416999155,1418644894,1,'1','在线客服','','','http://wpa.qq.com/msgrd?v=3&amp;uin=3435338256&amp;site=qq&amp;menu=yes','0',0),(59,1,1,'lnest_admin','/content/show//59',15,1416999155,1418644894,1,'1','帮助','','','/content/index/7','0',0),(60,1,52,'admin','/content/show//60',2,1479870391,1480172063,1,'3','产品中心','','','/cmsproduct','0',0),(61,1,52,'admin','/content/show//61',12,1480172910,1482564639,1,'3','在线客服','','','http://wpa.qq.com/msgrd?v=3&uin=564093832&site=qq&menu=yes','1',0);
/*!40000 ALTER TABLE `zz_nav` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:22
