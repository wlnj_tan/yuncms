-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_article`
--

DROP TABLE IF EXISTS `zz_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` int(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `title_style` varchar(40) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `keywords` varchar(120) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `url` varchar(60) NOT NULL DEFAULT '',
  `template` varchar(40) NOT NULL DEFAULT '',
  `posid` varchar(40) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `lang` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `publish` text NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`id`,`status`,`listorder`),
  KEY `catid` (`id`,`catid`,`status`),
  KEY `listorder` (`id`,`catid`,`status`,`listorder`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_article`
--

LOCK TABLES `zz_article` WRITE;
/*!40000 ALTER TABLE `zz_article` DISABLE KEYS */;
INSERT INTO `zz_article` VALUES (33,65,52,'admin','包装材料融入日常生活之别用非食用包装袋装食品','','[]','','','<p style=\"font-family:Verdana, Arial, Helvetica, sans-serif;color:#444444;font-size:14px;background-color:#FFFFFF;\">\r\n	不少市民习惯用非食品塑料袋包装食品，可是大家注意过这样的塑料袋是否对身体健康有害吗？\r\n</p>\r\n<p style=\"font-family:Verdana, Arial, Helvetica, sans-serif;color:#444444;font-size:14px;background-color:#FFFFFF;\">\r\n	为了健康，盛装食品的包装袋一定要是食品级别的。非食品包装袋苯等重金属超标，卫生指标也不合格，非食用包装袋包装的食品易致癌。\r\n</p>\r\n<p style=\"font-family:Verdana, Arial, Helvetica, sans-serif;color:#444444;font-size:14px;background-color:#FFFFFF;\">\r\n	目前市场上有不少早餐供应点和商家依然使用非食品用的塑料袋包装食品，而这些塑料包装袋大多是“三无”产品，使用这类塑料包装袋包装食品会危害消费者的身体健康。7月底，株洲市工商局将对市面上使用非食品包装袋的行为进行整治。\r\n</p>\r\n<p style=\"font-family:Verdana, Arial, Helvetica, sans-serif;color:#444444;font-size:14px;background-color:#FFFFFF;\">\r\n	有检测结果显示，非食品包装袋的主要材料是聚乙烯、聚丙烯、聚氯乙烯等，多数聚氯乙烯制成的塑料袋有毒，用它来包装食品，容易引发癌症。\r\n</p>','/content/show/65/33','show','',1,0,24,1480063670,1480065216,1,'',''),(34,65,52,'admin','纸袋“装”入丰富内涵','','[{\"path\":\"\\/upload\\/images\\/gallery\\/8\\/g\\/305_src.png\",\"title\":\"Picture26\"}]','','','<p style=\"color:#666666;font-family:微软雅黑, 宋体, &quot;font-size:14px;background-color:#FFFFFF;\">\r\n	到商场购物、去名牌店扫货、购买化妆品、参观展览会……总能看到人们的手里拎着印有各种标志的纸袋子。它们原本是装载物品以及商家宣传的手段，但随着制式越来越精美，而且具有环保和可回收利用的特点，这些纸袋子逐渐从商用领域扩展到了私人领域。\r\n</p>\r\n<p style=\"color:#666666;font-family:微软雅黑, 宋体, &quot;font-size:14px;background-color:#FFFFFF;\">\r\n	今年暑期，这股纸袋风越刮越强劲，很多商场都为它们开辟了专门的柜台，将印制精美、便携环保的纸质手提袋推上了时尚前沿。这些手提袋尺寸有的硕大，有的娇小；颜色可淡雅，也可亮艳；图案或标新立异，或古朴清新；既可在用作购物袋，又可在旅行时折叠携带，十分受消费者的欢迎。\r\n</p>\r\n<p style=\"color:#666666;font-family:微软雅黑, 宋体, &quot;font-size:14px;background-color:#FFFFFF;\">\r\n	其实，之前纸质手提袋的市场关注度并不高，用作手提包时比不上皮包的高贵气质；用作购物袋时，不如塑料袋来的方便，即便是沾些名牌血统曾经在中国大卖的Longchamp折叠手提袋，也只不过是巴黎女人们的“买菜包”，很难登上大雅之堂。不过，随着限塑令以及环保理念的逐渐流行，人们开始重新审视它们的优点，纸质手提袋也开始赢得越来越多<span style=\"line-height:23.8px;\">。</span> \r\n</p>\r\n<p style=\"color:#666666;font-family:微软雅黑, 宋体, &quot;font-size:14px;background-color:#FFFFFF;\">\r\n	<span style=\"line-height:1.7;\"> 市场上的很多手提袋都大有文章，它们在纸袋的原型上加入了更多的工艺，或翻新材质，或采用最新的流行图案，休闲随意而又简单利落。</span> \r\n</p>\r\n<p style=\"color:#666666;font-family:微软雅黑, 宋体, &quot;font-size:14px;background-color:#FFFFFF;\">\r\n	在淘宝网上，这些纸质手提袋也为数众多，在搜索栏输入“纸袋”字样后，会发现上万条商品信息，价格从几元到几百元不等，范围也涵盖了定做纸袋、礼品纸袋、时尚纸袋、专品纸袋等等。一家定做纸质手提袋的网店店主说，纸袋是一种比较小众的销售产品，除了部分商家或者公司定购，购买量一般不大。不过，随着个性化的发展，纸袋定做的趋势越来越明显，而且所接受的订单也越来越多。\r\n</p>\r\n<p style=\"color:#666666;font-family:微软雅黑, 宋体, &quot;font-size:14px;background-color:#FFFFFF;\">\r\n	在北京四道口金五星百货商场内的一家文具用品店面，各种时尚纸袋正在热销，很多印有卡通、风景等图案的时尚纸袋被摆在了柜台的显眼位置。正在柜台前挑选手提袋的80后消费者陈晓羽说，自己很喜欢使用和收藏手提袋，类似于小时候的火柴盒，而且这种袋子既美观又便宜，还可以作为礼品的包装袋送给亲朋好友。\r\n</p>\r\n<p style=\"color:#666666;font-family:微软雅黑, 宋体, &quot;font-size:14px;background-color:#FFFFFF;\">\r\n	虽说纸质手提袋的主要材质是纸，但是却也有细分，其中既包括比较厚重的白卡纸、压纹纸以及铜版纸、无光铜版纸，也包括韧性十足的牛皮纸、牛皮卡纸和胶版纸。除此之外，尼龙、麻布等材质的配饰也被运用其上，彰显了夏日的闲适自在和多用途性，而且加入扎染的渐变色彩效果，在兼具未来感与民族风的同时，也不失俏皮青春的活力。\r\n</p>\r\n<p style=\"color:#666666;font-family:微软雅黑, 宋体, &quot;font-size:14px;background-color:#FFFFFF;\">\r\n	另一方面，随着印刷技术和设备的现代化，手提袋的印刷图案也越来越精美、细腻，而且可以根据客户的需求，覆上薄的塑料膜，增加它的韧性和耐水性；甚至增加烫金、凹凸、UV、模切异型等专业设计。\r\n</p>\r\n<p style=\"color:#666666;font-family:微软雅黑, 宋体, &quot;font-size:14px;background-color:#FFFFFF;\">\r\n	中国礼品产业研究院应用国家统计局、商务部的行业数据，对礼品行业进行了个体与团体分类统计测算，得出的数据是，目前，我国个体的年礼品需求约在5055亿元，团体的年礼品需求约2629亿元，其中商务和福利礼品需求占其中1900多亿元。国内礼品市场年需求总额合计7684亿元左右，整体市场接近8000亿元规模。根据如此庞大的市场需求，可以推知作为大众通俗，而又个性化鲜明的外包装物品的纸质手提袋，其未来的发展空间将非常巨大。\r\n</p>\r\n<p style=\"color:#666666;font-family:微软雅黑, 宋体, &quot;font-size:14px;background-color:#FFFFFF;\">\r\n	北京某印刷有限公司的销售人员潘晶晶表示，公司接收的手提袋订单逐年上涨，特别是在限塑令实施之后，消费者的环保理念不断提升，相应的手提袋的使用量也随之增加。\r\n</p>\r\n<p style=\"color:#666666;font-family:微软雅黑, 宋体, &quot;font-size:14px;background-color:#FFFFFF;\">\r\n	与此同时，一些纸质手提袋产品还表现出追逐名牌效应的趋势，他们在外形上印上“LV”、“Chanel”、“GUCCI”等奢侈品牌的标志，仿制出奢侈品牌的购物袋。在淘宝网站上，记者搜索“名牌纸袋”后发现了800多条商品信息，这些产品的价格相差悬殊，低者少至1元，高者会达到100元，其中就以仿制品为主。而且，很多网店店主也毫不讳言自己销售的名牌购物袋是仿冒品，不过他们也会强调“质量是不错的，外形也很逼真”。\r\n</p>\r\n<p style=\"color:#666666;font-family:微软雅黑, 宋体, &quot;font-size:14px;background-color:#FFFFFF;\">\r\n	其实，不傍名牌的纸质手提袋也别有一番滋味，作为一种可回收再利用的资源，环保本身就是一种时尚。与其追逐超出自己消费能力的奢侈包包，还不如从平实的生活中寻求属于自己的文化价值。一款设计精美，又能表现心情的纸质购物袋，不需要多大牌的渲染，仍然可以体现消费者的不凡品位。《中国质量报》\r\n</p>','/content/show/65/34','show','',1,0,28,1480064065,1480065207,1,'',''),(35,65,52,'admin','信力纸业包装祝全体小伙伴们感恩节快乐','','[]','','','<p style=\"text-align:center;\">\r\n	<strong><span style=\"font-size:16px;\">为梦想，再出发！&nbsp;</span></strong>\r\n</p>\r\n<p>\r\n	&nbsp; &nbsp; &nbsp;各位亲爱的同事： &nbsp;2016年即将走过，回望过去，我们可以发现，通过大家的共同努力，信力取得了一定成绩！在此，感谢所有同仁对信力纸业包装的辛勤付出。 &nbsp;\r\n</p>\r\n<p>\r\n	&nbsp; &nbsp; &nbsp; 2016年注定是不平凡的一年，是充满机遇和挑战的一年。外部市场环境竞争激烈、我们也面对更多的困难。 现在，公司制定了新的战略决策和目标，需要我们稳步、高效的去实施。我坚信，志同道合的我们，凝聚在一起修炼内功，磨枪砺胆，抓住机遇，重塑价值实现和积极开放的文化氛围，铸就可传承的企业基因，我们一定会成为受人尊重的公司。&nbsp;\r\n</p>\r\n<p>\r\n	&nbsp; &nbsp; &nbsp; 各位同仁，未来的五年，将是大浪淘沙、适者生存的五年；我们每一位都要勇担责任，满怀激情；“雄关漫道真如铁，而今迈步从头越”，让我们为梦想，再出发！ &nbsp;\r\n</p>\r\n<p>\r\n	&nbsp; &nbsp; &nbsp; 最后，祝愿所有的同事及家人一切顺利，幸福安康！感恩节快乐！\r\n</p>','/content/show/65/35','show','',1,0,47,1480162481,0,1,'','');
/*!40000 ALTER TABLE `zz_article` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:22
