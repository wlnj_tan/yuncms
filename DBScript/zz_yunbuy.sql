-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_yunbuy`
--

DROP TABLE IF EXISTS `zz_yunbuy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_yunbuy` (
  `buy_id` int(10) NOT NULL AUTO_INCREMENT,
  `goods_id` int(10) NOT NULL,
  `sid` int(10) NOT NULL DEFAULT '0' COMMENT '源商品',
  `title` varchar(255) NOT NULL COMMENT '商品名称',
  `goods_subtit` varchar(255) NOT NULL DEFAULT '' COMMENT '港淘副标题',
  `cid` int(10) NOT NULL DEFAULT '0' COMMENT '分类ID',
  `cover` int(11) NOT NULL COMMENT '商品封面',
  `pics` varchar(60) NOT NULL COMMENT '图片列表',
  `goods_price` double(20,2) NOT NULL COMMENT '商品总价',
  `price` double(20,2) NOT NULL COMMENT '港淘单次价格',
  `custom_price` double(20,2) NOT NULL COMMENT '自定义商品价值',
  `need_num` int(10) NOT NULL COMMENT '总需人数',
  `buy_num` int(10) NOT NULL DEFAULT '0' COMMENT '已参与人数',
  `end_num` int(10) NOT NULL DEFAULT '0' COMMENT '剩余人数',
  `qishu` smallint(6) NOT NULL COMMENT '期数',
  `max_qishu` smallint(6) NOT NULL COMMENT '最大期数',
  `is_rec` smallint(3) NOT NULL DEFAULT '0' COMMENT '是否推荐',
  `member_id` int(11) NOT NULL DEFAULT '0' COMMENT '中奖用户ID',
  `member_name` varchar(255) NOT NULL DEFAULT '' COMMENT '中奖用户名',
  `luck_code` char(8) NOT NULL DEFAULT '0' COMMENT '中奖号码',
  `kjjg` varchar(12) NOT NULL DEFAULT '' COMMENT '时时彩开奖结果',
  `time_total` char(20) NOT NULL DEFAULT '' COMMENT '时间总和',
  `add_time` int(10) NOT NULL COMMENT '添加时间',
  `start_time` int(10) NOT NULL COMMENT '开始时间',
  `end_time` varchar(60) NOT NULL DEFAULT '' COMMENT '揭晓时间',
  `wait_time` int(10) DEFAULT NULL COMMENT '等待时间',
  `last_dbtime` varchar(60) NOT NULL DEFAULT '' COMMENT '最后港淘时间',
  `qihao` varchar(10) NOT NULL DEFAULT '' COMMENT '开奖期号',
  `is_show` varchar(3) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `goods_click` int(11) NOT NULL DEFAULT '0' COMMENT '点击量',
  `listorders` smallint(3) NOT NULL DEFAULT '0' COMMENT '排序',
  `keywords` varchar(120) NOT NULL DEFAULT '',
  `description` mediumtext,
  `type` smallint(3) DEFAULT '1' COMMENT '类型:1普通2增币',
  `posid` tinyint(3) DEFAULT NULL COMMENT '推荐位',
  `thumb` text COMMENT '推荐图',
  `ext_info` text,
  `buynum` int(11) DEFAULT '0' COMMENT '限购人数',
  `thumbs` text,
  `is_off` int(1) unsigned NOT NULL COMMENT '0',
  `gobuy` tinyint(1) DEFAULT '0' COMMENT '1直购商品',
  `mid` int(11) DEFAULT '0' COMMENT '商家会员ID',
  `bus_money` double(11,2) DEFAULT '0.00' COMMENT '商家分润金额',
  PRIMARY KEY (`buy_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_yunbuy`
--

LOCK TABLES `zz_yunbuy` WRITE;
/*!40000 ALTER TABLE `zz_yunbuy` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_yunbuy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:19
