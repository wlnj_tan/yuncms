-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_virtual`
--

DROP TABLE IF EXISTS `zz_virtual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_virtual` (
  `vir_id` bigint(32) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '币类型（1余额、2夺宝币）',
  `order_item_id` int(11) DEFAULT '0' COMMENT '订单详细表ID',
  `vir_number` varchar(32) NOT NULL COMMENT '卡号',
  `vir_pass` varchar(255) NOT NULL COMMENT '卡密',
  `vir_status` tinyint(1) DEFAULT '0' COMMENT '状态（0未发放 1已发放 2已使用）',
  `vir_time_add` int(11) DEFAULT NULL COMMENT '发放时间',
  `vir_time_end` int(11) DEFAULT NULL COMMENT '有效时间',
  `vir_time_win` int(11) DEFAULT NULL COMMENT '领取时间',
  `vir_time_use` int(11) DEFAULT NULL COMMENT '使用时间',
  `vir_mid` int(11) DEFAULT '0' COMMENT '发放的会员',
  `vir_username` varchar(50) DEFAULT NULL COMMENT '发放的会员',
  PRIMARY KEY (`vir_id`),
  KEY `goods_id` (`goods_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_virtual`
--

LOCK TABLES `zz_virtual` WRITE;
/*!40000 ALTER TABLE `zz_virtual` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_virtual` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:17
