-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_business`
--

DROP TABLE IF EXISTS `zz_business`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_business` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL DEFAULT '0' COMMENT '会员ID',
  `bus_name` varchar(200) NOT NULL COMMENT '商家名称',
  `bus_company` varchar(200) DEFAULT NULL COMMENT '公司名称',
  `bus_company_type` varchar(255) DEFAULT NULL COMMENT '实体类型',
  `bus_qq` varchar(20) DEFAULT NULL COMMENT '客服QQ',
  `bus_address` varchar(200) DEFAULT NULL COMMENT '公司地址',
  `bus_zone` int(10) DEFAULT '0' COMMENT '区域ID',
  `bus_area` varchar(100) DEFAULT NULL COMMENT '区域',
  `bus_status` tinyint(2) DEFAULT '0' COMMENT '-1审核未通过 0审核中 10审核成功 20店铺关闭',
  `bus_times` tinyint(2) DEFAULT '0' COMMENT '申请入驻次数',
  `time_apply` int(11) DEFAULT NULL COMMENT '申请时间',
  `time_open` int(11) DEFAULT NULL COMMENT '开店时间',
  `bus_why` text COMMENT '备注',
  `bus_logo` varchar(255) DEFAULT NULL COMMENT '店铺LOGO',
  `bus_intro` text COMMENT '商家介绍',
  `bus_ads` text COMMENT '广告图',
  `bus_scope` varchar(255) DEFAULT NULL COMMENT '经营范围',
  `bus_tel` varchar(20) DEFAULT NULL COMMENT '客服电话',
  `bus_limit` int(8) DEFAULT '0' COMMENT '默认商品发布上限',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_business`
--

LOCK TABLES `zz_business` WRITE;
/*!40000 ALTER TABLE `zz_business` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_business` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:25
