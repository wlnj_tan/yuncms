-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_page`
--

DROP TABLE IF EXISTS `zz_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_page` (
  `id` int(11) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `userid` int(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `url` varchar(60) NOT NULL DEFAULT '',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `lang` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `title_style` varchar(40) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_page`
--

LOCK TABLES `zz_page` WRITE;
/*!40000 ALTER TABLE `zz_page` DISABLE KEYS */;
INSERT INTO `zz_page` VALUES (1,1,2,'admin','',0,1405472290,1405472293,1,'关于我们','','','asdfasdf',0),(2,1,2,'admin','/content/show//2',0,1405409998,1483539415,1,'公司简介','','','<img src=\"/upload/cf2.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p style=\"font-family:微软雅黑;color:#555555;font-size:14px;text-indent:25px;\">\r\n	<br />\r\n</p>\r\n<p style=\"font-family:微软雅黑;color:#555555;font-size:14px;text-indent:25px;\">\r\n	开平市信力纸业包装有限公司成立于2003年8月12日，位于广东省开平市苍城镇东郊工业区，地理位置十分优越、交通非常便利。\r\n</p>\r\n<p style=\"font-family:微软雅黑;color:#555555;font-size:14px;text-indent:25px;\">\r\n	本公司集研发设计、生产和销售各种瓦楞箱和彩盒一体化的专业生产厂家，经过多年的积累和打拼，公司发展逐渐壮大并形成一支稳定且技术扎实的工作团队，拥有多名高专业素质的技术人员和管理人员，为了适应市场和客户的需求陆续购进纸箱和彩盒全套生产设备，配有三色、四色中国东方高速模切一体水墨印刷机、德国高宝五色对开、四色全张彩色胶印机、全自动啤机、全自动裱纸机、全自动粘盒机等高速先进生产设备，能充分满足客户的需求。\r\n</p>\r\n<p style=\"font-family:微软雅黑;color:#555555;font-size:14px;text-indent:25px;\">\r\n	凭借“信誉至上、质量第一、客户满意、不断超越”的经营宗旨，多年来一直保持品质高、交期准的良好传统，在与广大客户紧密合作的过程中，赢得了持续的信赖和好评，本公司将以更完美的产品，更可靠的质量，更完善的服务答谢新旧客户的支持与厚爱。\r\n</p>\r\n<p style=\"font-family:微软雅黑;color:#555555;font-size:14px;text-indent:25px;\">\r\n	在持续发展的过程中，本公司陆续引入了6S现场管理、IS09001：2008国际质量管理体系认证等先进的管理工具，对内弘扬企业文化，贯彻质量体系;对外传达企业精神,树立品牌概念。从而使企业的动作更加成熟和完善，并为取得更大发展打造了坚实的基础。\r\n</p>\r\n<p style=\"font-family:微软雅黑;color:#555555;font-size:14px;text-indent:25px;\">\r\n	<strong>本公司承诺：精益求精，满足并超越客户的期望！</strong> \r\n</p>\r\n<br />\r\n<p style=\"text-align:center;color:#333333;\">\r\n<img style=\"width:150px;height:150px;\" alt=\"\" src=\"/upload/qrcode2small.jpg\"> \r\n	</p>',2044),(11,1,2,'admin','',0,1404271493,1404271496,1,'网站地图','','','网站地图',0),(18,1,1,'lnest_admin','',0,1404956243,1404956243,1,'红枣类','','','',0),(19,1,1,'lnest_admin','',0,1404956243,1404956243,1,'其它类','','','',0),(20,1,1,'lnest_admin','',0,1404956261,1404956261,1,'红飞健康枣系列','','','',0),(21,1,1,'lnest_admin','',0,1404956261,1404956261,1,'雪域圣果系列','','','',0),(22,1,1,'lnest_admin','',0,1404956261,1404956261,1,'枣博士系列','','','',0),(23,1,1,'lnest_admin','',0,1404956261,1404956261,1,'礼盒系列','','','',0),(24,1,1,'lnest_admin','',0,1404956261,1404956261,1,'枣片系列','','','',0),(25,1,1,'lnest_admin','',0,1404956261,1404956261,1,'枣干系列','','','',0),(26,1,1,'lnest_admin','',0,1404956261,1404956261,1,'蜜饯系列','','','',0),(27,1,1,'lnest_admin','',0,1404956281,1404956281,1,'红飞健康枣系列','','','',0),(28,1,1,'lnest_admin','',0,1404956281,1404956281,1,'雪域圣果系列','','','',0),(77,1,2,'admin','/content/show//77',0,1418644582,1483537909,1,'联系我们','','','<p style=\"color:#555555;font-family:&quot;font-size:14px;background-color:#FFFFFF;\">\r\n	<span style=\"color:#444444;font-family:Arial;font-size:10.5pt;\"> </span> \r\n</p>\r\n<div class=\"ny_contact_left\" style=\"margin:0px auto;padding:0px;text-align:center;color:#555555;font-family:宋体, Arial, Helvetica, sans-serif;\">\r\n	<p id=\"ny_ct_list\" style=\"text-align:left;color:#29415F;text-indent:20px;font-family:微软雅黑;font-size:18px;\">\r\n		开平市信力纸业包装有限公司\r\n	</p>\r\n	<p style=\"text-align:left;color:#333333;text-indent:20px;font-family:微软雅黑;\">\r\n		<span style=\"font-size:13px;\">联系人： 梁小姐</span> \r\n	</p>\r\n	<p style=\"text-align:left;color:#333333;text-indent:20px;font-family:微软雅黑;font-size:14px;\">\r\n		<span style=\"font-size:13px;\">手机：&nbsp;</span><span style=\"font-size:12px;\">18125364805</span> \r\n	</p>\r\n	<p style=\"text-align:left;color:#333333;text-indent:20px;font-family:微软雅黑;font-size:14px;\">\r\n		<span style=\"font-size:13px;\">固话：&nbsp;</span><span style=\"font-size:12px;\">0750-2828088/2822888</span> \r\n	</p>\r\n	<p style=\"text-align:left;color:#333333;text-indent:20px;font-family:微软雅黑;font-size:14px;\">\r\n		<span style=\"font-size:13px;\">传真：&nbsp;</span><span style=\"font-size:12px;\">0750-2821111</span> \r\n	</p>\r\n	<p style=\"text-align:left;color:#333333;text-indent:20px;font-family:微软雅黑;font-size:14px;\">\r\n		<span style=\"font-size:13px;\">QQ：&nbsp;</span><span style=\"font-size:12px;\">564093832</span> \r\n	</p>\r\n	<p style=\"text-align:left;color:#333333;text-indent:20px;font-family:微软雅黑;font-size:14px;\">\r\n		<span style=\"font-size:13px;\">Email：&nbsp;</span><span style=\"font-size:12px;\">564093832@qq.com</span> \r\n	</p>\r\n	<p style=\"text-align:left;color:#333333;text-indent:20px;font-family:微软雅黑;font-size:14px;\">\r\n		<span style=\"font-size:13px;\">地址：&nbsp;</span><span style=\"font-size:12px;\">开平市苍城镇东郊工业区</span> \r\n	</p>\r\n	<p style=\"text-align:left;color:#333333;text-indent:30px;\">\r\n<img style=\"width:150px;height:150px;\" alt=\"\" src=\"/upload/qrcode2small.jpg\" /> \r\n	</p>\r\n</div>\r\n<div class=\"ny_contact_right\" style=\"margin:0px auto;padding:0px;text-align:center;color:#555555;font-family:宋体, Arial, Helvetica, sans-serif;\">\r\n	<img style=\"width:309px;height:200px;\" alt=\"\" src=\"/images/earth.png\" /> \r\n</div>\r\n<p>\r\n	<br />\r\n</p>',3155),(80,1,1,'lnest_admin','',0,1432287386,1432287386,1,'官方交流群','','','',272),(93,1,52,'admin','/content/show//93',0,1480177903,1483107434,1,'荣誉证书','','','<p>\r\n	质量管理体系认证证书：\r\n</p>\r\n<p>\r\n	<img src=\"/upload/ISO1.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n</p>\r\n<p>\r\n	&nbsp;\r\n</p>\r\n<p>\r\n	<img src=\"/upload/ISO2.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	营业执照：\r\n</p>\r\n<p>\r\n	<img src=\"/upload/zs2.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n</p>\r\n<p>\r\n	&nbsp;\r\n</p>\r\n<p>\r\n	<img src=\"/upload/zs3.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	守合同重信用企业：\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<img src=\"/upload/zs4.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n</p>\r\n<p>\r\n	印刷经营许可证：\r\n</p>\r\n<p>\r\n	<img src=\"/upload/zs5.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n</p>',32),(94,1,52,'admin','/content/show//94',0,1480178312,1483539540,1,'厂房参观','','','<img src=\"/upload/gs2.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/gs1.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/gs3.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/machine01.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/machine02.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/machine03.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/machine04.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/machine05.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/machine06.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/machine07.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/gs4.jpg\" alt=\"\" style=\"width:100%;\" />',28),(95,1,52,'admin','/content/show//95',0,1483457639,1483457673,1,'检测设备','','','<img src=\"/upload/machineTest01.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/machineTest02.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/machineTest03.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n',10),(96,1,52,'admin','/content/show//96',0,1483457681,1483457717,1,'礼盒机器','','','<img src=\"/upload/machineLP01.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/machineLP02.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n<p>\r\n	&nbsp;\r\n</p>\r\n<img src=\"/upload/machineLP03.jpg\" alt=\"\" style=\"width:100%;\" /> \r\n',12),(99,1,52,'admin','/content/show//99',0,1485014128,1485014161,1,'招贤纳士','','','招聘信息',5);
/*!40000 ALTER TABLE `zz_page` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:16
