-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_goods_order`
--

DROP TABLE IF EXISTS `zz_goods_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_goods_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mid` int(11) DEFAULT NULL COMMENT '会员ID',
  `note` varchar(300) DEFAULT NULL COMMENT '用户备注',
  `status` tinyint(4) DEFAULT '1' COMMENT '订单状态,1已下单等待付款,2已付款等待发货,3已发货填入物流单号,4已签收完成交易,5交易取消',
  `order_price` decimal(10,2) DEFAULT NULL COMMENT '实际购买的价格',
  `pay_time` int(10) DEFAULT '0' COMMENT '付款时间',
  `express` varchar(40) DEFAULT NULL COMMENT '物流类型',
  `express_num` varchar(64) DEFAULT NULL COMMENT '订单号',
  `express_send_time` int(11) DEFAULT NULL,
  `order_sn` varchar(30) DEFAULT NULL,
  `is_evaluate` tinyint(2) DEFAULT '0' COMMENT '是否已评论,状态为4时有效',
  `credit` int(11) DEFAULT '0' COMMENT '本次订单可赠积分',
  `c_time` int(11) DEFAULT NULL COMMENT '创建订单时间',
  `extension_code` varchar(100) DEFAULT NULL,
  `extension_id` int(11) DEFAULT NULL,
  `integral` int(10) DEFAULT '0' COMMENT '拍币',
  `surplus` decimal(10,2) DEFAULT '0.00' COMMENT '余额',
  `amount` decimal(10,2) DEFAULT '0.00' COMMENT '未支付金额',
  `pay_fee` decimal(10,2) DEFAULT NULL COMMENT '第三方支付金额',
  `pay_id` tinyint(5) DEFAULT '0' COMMENT '支付方式ID',
  `pay_name` varchar(200) DEFAULT NULL COMMENT '支付方式名',
  `deposit` decimal(10,0) DEFAULT '0' COMMENT '使用冻结款',
  `deliver` varchar(500) DEFAULT NULL COMMENT '收获地址',
  `mobile` varchar(100) DEFAULT NULL,
  `area` varchar(200) DEFAULT NULL COMMENT '区域',
  `name` varchar(200) DEFAULT NULL COMMENT '收货 人',
  `is_share` smallint(3) DEFAULT '0' COMMENT '是否晒单',
  `money_paid` decimal(10,2) DEFAULT NULL COMMENT '第三方支付金额',
  `oldprice` decimal(10,2) DEFAULT NULL COMMENT '采购价',
  `fow` varchar(100) DEFAULT NULL COMMENT '采购来源',
  `fono` varchar(200) DEFAULT NULL COMMENT '采购订单号',
  `fou` text COMMENT '采购站点标识',
  `ismoney` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1账务已确认',
  `add_bonus_id` int(11) NOT NULL DEFAULT '0' COMMENT '赠送的抵用券ID',
  `bus_mid` int(11) DEFAULT '0' COMMENT '商家会员ID',
  `bus_money_is` tinyint(1) DEFAULT '0' COMMENT '1已分润',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_goods_order`
--

LOCK TABLES `zz_goods_order` WRITE;
/*!40000 ALTER TABLE `zz_goods_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_goods_order` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:19
