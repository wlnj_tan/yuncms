-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_auction_log`
--

DROP TABLE IF EXISTS `zz_auction_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_auction_log` (
  `log_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` mediumint(8) unsigned NOT NULL,
  `bid_user` mediumint(8) unsigned NOT NULL,
  `bid_price` decimal(10,2) unsigned NOT NULL,
  `bid_time` decimal(20,3) unsigned NOT NULL,
  `first` int(5) DEFAULT '1' COMMENT '第几次出价',
  `cod` varchar(10) DEFAULT NULL COMMENT '中奖随机码',
  `cod_true` varchar(10) NOT NULL DEFAULT '' COMMENT '当期中奖号',
  `status` int(1) DEFAULT '0' COMMENT '0默认状态 -1未中奖 1已中奖 2已领奖',
  `cod_time` decimal(20,0) DEFAULT NULL COMMENT '中奖时间',
  `frozen` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未解冻',
  `win` decimal(5,2) NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT '1',
  `ip` varchar(50) DEFAULT NULL,
  `fdis` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1永不失效',
  PRIMARY KEY (`log_id`,`cod_true`),
  KEY `act_id` (`act_id`),
  KEY `idx_bid_user` (`bid_user`),
  KEY `idx_status` (`status`),
  KEY `idx_first` (`first`),
  KEY `idx_id_u_t` (`act_id`,`bid_user`,`bid_time`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_auction_log`
--

LOCK TABLES `zz_auction_log` WRITE;
/*!40000 ALTER TABLE `zz_auction_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_auction_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:15
