-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_category`
--

DROP TABLE IF EXISTS `zz_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_category` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `catname` varchar(30) NOT NULL DEFAULT '',
  `catdir` varchar(30) DEFAULT NULL,
  `parentdir` varchar(50) DEFAULT NULL,
  `parentid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `moduleid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `module` char(24) DEFAULT NULL,
  `arrparentid` text,
  `arrchildid` text,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(150) DEFAULT NULL,
  `keywords` varchar(100) DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ishtml` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(255) DEFAULT NULL,
  `child` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `url` varchar(150) DEFAULT NULL,
  `template_list` varchar(20) DEFAULT NULL,
  `template_show` varchar(20) DEFAULT NULL,
  `pagesize` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `readgroup` varchar(100) NOT NULL DEFAULT '0',
  `listtype` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lang` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `urlruleid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`),
  KEY `listorder` (`listorder`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_category`
--

LOCK TABLES `zz_category` WRITE;
/*!40000 ALTER TABLE `zz_category` DISABLE KEYS */;
INSERT INTO `zz_category` VALUES (1,'关于信力','','',0,0,'','0','1,90,2,95,96,93,94,91,77,99,92',1,'','','',0,0,1,0,'',1,'/content/index/2','','',0,'',0,1,0),(2,'公司简介','','',90,1,'page','0,1,90','2',0,'','','',5,0,1,0,'',0,'/content/index/2','index','',0,'',1,1,0),(77,'联系方式',NULL,NULL,91,1,'page','0,1,91','77',0,'','','',0,0,1,0,'',0,'/content/index/77','index','',10,'0',1,1,0),(95,'检测设备',NULL,NULL,90,1,'page','0,1,90','95',0,'检测设备','检测设备','检测设备',0,0,1,0,'',0,'/content/index/95','','',20,'0',1,1,0),(99,'招贤纳士',NULL,NULL,91,1,'page','0,1,91','99',0,'招贤纳士','信力招贤纳士','信力招贤纳士',0,0,1,0,'',0,'/content/index/99','','',20,'0',1,1,0),(96,'礼盒机器',NULL,NULL,90,1,'page','0,1,90','96',0,'礼盒机器','礼盒机器','礼盒机器',0,0,1,0,'',0,'/content/index/96','','',20,'0',1,1,0),(65,'新闻资讯',NULL,NULL,0,2,'article','0','65',0,'','','',0,0,1,0,'',0,'/content/index/65','list','show',10,'0',0,1,0),(90,'走进信力',NULL,NULL,1,0,'','0,1','90,2,95,96,93,94',1,'','','',0,0,1,0,'',1,'/content/index/2','','',20,'0',0,1,0),(91,'联系我们',NULL,NULL,1,0,'','0,1','91,77,99,92',1,'','','',0,0,1,0,'',1,'/content/index/77','','',20,'0',0,1,0),(92,'在线客服',NULL,NULL,91,0,'','0,1,91','92',1,'','','',0,0,1,0,'',0,'http://wpa.qq.com/msgrd?v=3&uin=564093832&site=qq&menu=yes','','',20,'0',0,1,0),(93,'荣誉证书',NULL,NULL,90,1,'page','0,1,90','93',0,'','','',0,0,1,0,'',0,'/content/index/93','index','',20,'0',1,1,0),(94,'厂房参观',NULL,NULL,90,1,'page','0,1,90','94',0,'','','',0,0,1,0,'',0,'/content/index/94','','',20,'0',1,1,0);
/*!40000 ALTER TABLE `zz_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:17
