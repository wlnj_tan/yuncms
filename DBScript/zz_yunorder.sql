-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_yunorder`
--

DROP TABLE IF EXISTS `zz_yunorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_yunorder` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_sn` varchar(60) NOT NULL COMMENT '订单编号',
  `mid` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `pay_points` int(11) NOT NULL DEFAULT '0' COMMENT '支付积分',
  `db_points` int(10) NOT NULL DEFAULT '0' COMMENT '淘币',
  `db_scores` int(10) NOT NULL DEFAULT '0' COMMENT '福分',
  `user_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '可用余额',
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '优惠',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单总额',
  `order_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '应付金额',
  `pay_id` int(11) NOT NULL DEFAULT '0' COMMENT '支付ID',
  `pay_name` varchar(30) NOT NULL DEFAULT '' COMMENT '支付名称',
  `pay_code` varchar(30) NOT NULL DEFAULT '' COMMENT '支付代码',
  `db_time` varchar(30) NOT NULL DEFAULT '' COMMENT '港淘时间微秒',
  `add_time` int(10) NOT NULL COMMENT '添加时间',
  `status` smallint(3) DEFAULT '1' COMMENT '状态:1未支付2已支付',
  `type` smallint(3) DEFAULT '1' COMMENT '类型',
  `sharecode` char(5) NOT NULL DEFAULT '' COMMENT '分享码',
  `used_sharecode` char(5) NOT NULL DEFAULT '' COMMENT '使用过分享码',
  `user_bonus_id` varchar(255) NOT NULL DEFAULT '0' COMMENT '发放的抵用券ID',
  `status_buy` tinyint(1) DEFAULT '0' COMMENT '0无直购商品 1直购商品未付款 2直购商品已付款',
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `order_id` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_yunorder`
--

LOCK TABLES `zz_yunorder` WRITE;
/*!40000 ALTER TABLE `zz_yunorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_yunorder` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:24
