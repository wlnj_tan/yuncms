-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_member_other`
--

DROP TABLE IF EXISTS `zz_member_other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_member_other` (
  `mid` int(11) unsigned NOT NULL,
  `isMob` tinyint(1) NOT NULL DEFAULT '0' COMMENT '绑定手机是否领取',
  `isMail` tinyint(1) NOT NULL DEFAULT '0' COMMENT '邮箱认证是否领取',
  `isDaren` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '港淘达人是否领取',
  `isStep` tinyint(1) NOT NULL DEFAULT '0' COMMENT '安装并登陆一元港淘港淘和竟拍是否领取',
  `isJpDaren` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '竞拍达人是否领取',
  `isPhoto` tinyint(1) NOT NULL DEFAULT '0' COMMENT '上传图像是否领取',
  `isVoice` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否语音认证',
  `isIdcard` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否实名认证',
  `isNickName` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否修改昵称',
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_member_other`
--

LOCK TABLES `zz_member_other` WRITE;
/*!40000 ALTER TABLE `zz_member_other` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_member_other` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:21
