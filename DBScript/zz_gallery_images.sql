-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_gallery_images`
--

DROP TABLE IF EXISTS `zz_gallery_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_gallery_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=470 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_gallery_images`
--

LOCK TABLES `zz_gallery_images` WRITE;
/*!40000 ALTER TABLE `zz_gallery_images` DISABLE KEYS */;
INSERT INTO `zz_gallery_images` VALUES (228,4),(247,1),(229,4),(216,4),(218,4),(232,5),(233,5),(217,4),(73,1),(70,1),(66,1),(72,1),(69,1),(74,1),(76,1),(78,1),(79,0),(80,0),(81,0),(82,1),(83,1),(84,1),(85,1),(86,1),(87,0),(88,1),(89,1),(90,0),(91,0),(92,1),(251,8),(221,1),(234,5),(261,1),(114,1),(231,4),(135,1),(227,4),(248,1),(164,1),(230,4),(215,4),(250,8),(249,8),(220,4),(235,5),(61,2),(162,2),(236,5),(237,6),(238,6),(242,7),(241,7),(243,7),(252,8),(246,7),(253,9),(254,9),(255,9),(256,9),(257,9),(258,9),(259,9),(262,10),(263,1),(264,10),(265,10),(266,10),(267,10),(268,10),(269,10),(270,10),(271,5),(272,5),(273,5),(274,4),(275,4),(276,4),(277,1),(281,6),(282,6),(283,6),(284,1),(285,1),(286,1),(287,1),(300,1),(291,1),(293,1),(301,1),(297,1),(298,1),(302,1),(303,1),(304,1),(305,1),(306,1),(307,1),(308,1),(309,1),(310,1),(311,1),(312,1),(313,1),(314,1),(315,1),(316,1),(317,1),(318,1),(319,1),(320,1),(321,1),(322,1),(323,1),(324,1),(325,1),(326,1),(327,1),(328,1),(329,1),(330,1),(331,1),(332,1),(333,1),(334,1),(335,1),(336,1),(337,1),(338,1),(339,1),(340,1),(341,1),(342,1),(343,1),(344,1),(345,1),(346,1),(347,1),(348,1),(349,1),(350,1),(351,1),(352,1),(353,1),(354,1),(355,1),(356,1),(357,1),(358,1),(359,1),(360,1),(361,1),(362,1),(363,1),(364,1),(365,1),(366,1),(367,1),(368,1),(369,1),(370,1),(371,1),(372,1),(373,1),(374,1),(375,1),(376,1),(377,1),(378,1),(379,1),(466,1),(465,1),(464,1),(463,1),(462,1),(461,1),(460,1),(459,1),(458,1),(457,1),(456,1),(455,1),(454,1),(453,1),(452,1),(451,1),(450,1),(449,1),(448,1),(447,1),(446,1),(445,1),(444,1),(443,1),(442,1),(441,1),(440,1),(439,1),(438,1),(437,1),(436,1),(435,1),(434,1),(433,1),(432,1),(431,1),(430,1),(429,1),(428,1),(427,1),(426,1),(425,1),(424,1),(423,1),(467,1),(468,1),(469,1);
/*!40000 ALTER TABLE `zz_gallery_images` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:22
