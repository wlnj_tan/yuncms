-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_yuncat`
--

DROP TABLE IF EXISTS `zz_yuncat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_yuncat` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `catname` varchar(255) NOT NULL COMMENT '分类名称',
  `parentid` int(10) NOT NULL DEFAULT '0' COMMENT '父级ID',
  `arrparentid` text NOT NULL COMMENT '父类ID组',
  `arrchildid` text NOT NULL COMMENT '子类ID组',
  `child` tinyint(1) NOT NULL COMMENT '是否有子级',
  `title` varchar(60) NOT NULL COMMENT '页面标题',
  `keywords` varchar(120) NOT NULL COMMENT '页面关键字',
  `description` int(255) NOT NULL COMMENT '页面描述',
  `listorder` smallint(5) NOT NULL DEFAULT '0' COMMENT '排序',
  `thumb` varchar(255) NOT NULL COMMENT '缩略图',
  `url` varchar(255) NOT NULL COMMENT 'URL',
  `ismenu` smallint(3) NOT NULL DEFAULT '0' COMMENT '是否导航',
  `catname_en` varchar(100) DEFAULT NULL,
  `iconfont` varchar(20) DEFAULT NULL COMMENT '字体图标',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_yuncat`
--

LOCK TABLES `zz_yuncat` WRITE;
/*!40000 ALTER TABLE `zz_yuncat` DISABLE KEYS */;
INSERT INTO `zz_yuncat` VALUES (1,'纸箱包装',0,'0','1',0,'','',0,9,'[{\"path\":\"\\/upload\\/images\\/gallery\\/8\\/h\\/306_src.jpg\",\"title\":\"img5\"}]','/yunbuy/index/?cid=1',1,'',NULL),(2,'精品礼盒',0,'0','2',0,'','',0,8,'[{\"path\":\"\\/upload\\/images\\/gallery\\/8\\/i\\/307_src.jpg\",\"title\":\"IMG4\"}]','/yunbuy/index/?cid=2',1,'',NULL),(3,'彩盒印刷',0,'0','3',0,'','',0,7,'[{\"path\":\"\\/upload\\/images\\/gallery\\/8\\/j\\/308_src.jpg\",\"title\":\"IMG3\"}]','/yunbuy/index/?cid=3',1,'',NULL),(4,'礼盒定制',0,'0','4',0,'','',0,6,'[{\"path\":\"\\/upload\\/images\\/gallery\\/8\\/k\\/309_src.jpg\",\"title\":\"IMG2\"}]','/yunbuy/index/?cid=4',1,'',NULL),(5,'个护化妆',0,'0','5',0,'','',0,5,'','/yunbuy/index/?cid=5',0,'',NULL),(6,'母婴/营养保健',0,'0','6',0,'','',0,4,'','/yunbuy/index/?cid=6',0,'',NULL),(7,'快速专区',0,'0','7',0,'','',0,3,'','/yunbuy/index/?cid=7',0,'',NULL),(8,'旅游专区',0,'0','8',0,'','',0,2,'','/yunbuy/index/?cid=8',0,'',NULL),(9,'其他商品',0,'0','9',0,'','',0,1,'','/yunbuy/index/?cid=9',0,'',NULL);
/*!40000 ALTER TABLE `zz_yuncat` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:24
