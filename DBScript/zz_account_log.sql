-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_account_log`
--

DROP TABLE IF EXISTS `zz_account_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_account_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `mid` int(10) NOT NULL COMMENT '会员ID',
  `username` varchar(60) NOT NULL DEFAULT '' COMMENT '用户名',
  `pay_points` int(10) NOT NULL DEFAULT '0' COMMENT '支付积分',
  `rank_points` int(10) NOT NULL DEFAULT '0' COMMENT '等级积分',
  `db_points` int(10) DEFAULT '0' COMMENT '淘币',
  `db_scores` int(10) DEFAULT '0' COMMENT '福分',
  `user_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '可用金额',
  `frozen_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '冻结金额',
  `give_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '充值赠送',
  `stage` varchar(10) NOT NULL COMMENT '操作阶段',
  `desc` varchar(255) NOT NULL,
  `logtime` int(10) NOT NULL COMMENT '操作时间',
  `pay_points_total` int(10) DEFAULT NULL,
  `rank_points_total` int(10) DEFAULT NULL,
  `db_points_total` int(10) DEFAULT NULL,
  `db_scores_total` int(10) DEFAULT NULL,
  `user_money_total` decimal(10,2) DEFAULT NULL,
  `frozen_money_total` decimal(10,2) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT '0.00' COMMENT '第三方支付金额',
  `commission` int(10) DEFAULT '0' COMMENT '佣金',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `mid` (`mid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_account_log`
--

LOCK TABLES `zz_account_log` WRITE;
/*!40000 ALTER TABLE `zz_account_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_account_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:19
