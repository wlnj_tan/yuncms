-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_ad`
--

DROP TABLE IF EXISTS `zz_ad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_ad` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `userid` int(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `url` varchar(60) NOT NULL DEFAULT '',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `lang` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `title_style` varchar(40) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `width` text NOT NULL,
  `height` text NOT NULL,
  `images` mediumtext NOT NULL,
  `bgcolor` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_ad`
--

LOCK TABLES `zz_ad` WRITE;
/*!40000 ALTER TABLE `zz_ad` DISABLE KEYS */;
INSERT INTO `zz_ad` VALUES (1,1,2,'admin','/content/show//1',0,1474103310,1482573230,1,'【首页】主导航下焦点图','','[]','730','600','[{\"path\":\"\\/upload\\/images\\/gallery\\/c\\/z\\/468_src.jpg\",\"title\":\"#\"},{\"path\":\"\\/upload\\/images\\/gallery\\/8\\/e\\/303_src.jpg\",\"title\":\"#\"},{\"path\":\"\\/upload\\/images\\/gallery\\/8\\/d\\/302_src.jpg\",\"title\":\"#\"},{\"path\":\"\\/upload\\/images\\/gallery\\/8\\/c\\/301_src.jpg\",\"title\":\"#\"},{\"path\":\"\\/upload\\/images\\/gallery\\/8\\/b\\/300_src.jpg\",\"title\":\"#\"},{\"path\":\"\\/upload\\/images\\/gallery\\/8\\/f\\/304_src.jpg?123\",\"title\":\"#\"}]',''),(19,1,52,'admin','/content/show//19',0,1477375454,1477379058,1,'【APP】主导航下焦点图 ','','[]','740','300','[{\"path\":\"\\/upload\\/images\\/gallery\\/1\\/a\\/47_src.png\",\"title\":\"#\\/yunbuy?q=iphone6s\"},{\"path\":\"\\/upload\\/images\\/gallery\\/7\\/8\\/261_src.png\",\"title\":\"#\\/yunbuy?q=iphone 7\"}]',''),(7,1,1,'lnest_admin','/content/show//7',0,1417483084,1446443230,1,'【晒单专区】导航下横幅广告位','','[]','','200','[{\"path\":\"\\/upload\\/images\\/gallery\\/0\\/2\\/3_src.jpg\",\"title\":\"690_src\"}]',''),(8,1,1,'lnest_admin','/content/show//8',0,1474103291,1474103172,1,'【一元港淘】封面导航下横幅广告位','','[]','','205','[]','#2cda99'),(10,1,2,'admin','/content/show//10',0,1474103303,1482573420,1,'【微信】主导航下焦点图','','[]','740','300','[{\"path\":\"\\/upload\\/images\\/gallery\\/d\\/0\\/469_src.jpg\",\"title\":\"#\"},{\"path\":\"\\/upload\\/images\\/gallery\\/9\\/7\\/332_src.jpg\",\"title\":\"#\"},{\"path\":\"\\/upload\\/images\\/gallery\\/9\\/6\\/331_src.jpg\",\"title\":\"#\"},{\"path\":\"\\/upload\\/images\\/gallery\\/9\\/5\\/330_src.jpg\",\"title\":\"#\"},{\"path\":\"\\/upload\\/images\\/gallery\\/9\\/4\\/329_src.jpg\",\"title\":\"#\"},{\"path\":\"\\/upload\\/images\\/gallery\\/9\\/8\\/333_src.jpg?123\",\"title\":\"#\"}]','');
/*!40000 ALTER TABLE `zz_ad` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:21
