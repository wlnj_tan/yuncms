-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_mobilecat`
--

DROP TABLE IF EXISTS `zz_mobilecat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_mobilecat` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `catname` varchar(255) NOT NULL COMMENT '分类名称',
  `parentid` int(10) NOT NULL DEFAULT '0' COMMENT '父级ID',
  `arrparentid` text NOT NULL COMMENT '父类ID组',
  `arrchildid` text NOT NULL COMMENT '子类ID组',
  `child` tinyint(1) NOT NULL COMMENT '是否有子级',
  `title` varchar(60) NOT NULL COMMENT '页面标题',
  `keywords` varchar(120) NOT NULL COMMENT '页面关键字',
  `description` int(255) NOT NULL COMMENT '页面描述',
  `listorder` smallint(5) NOT NULL DEFAULT '0' COMMENT '排序',
  `thumb` varchar(255) NOT NULL COMMENT '缩略图',
  `url` varchar(255) NOT NULL COMMENT 'URL',
  `ismenu` smallint(3) NOT NULL DEFAULT '0' COMMENT '是否导航',
  `catname_en` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_mobilecat`
--

LOCK TABLES `zz_mobilecat` WRITE;
/*!40000 ALTER TABLE `zz_mobilecat` DISABLE KEYS */;
INSERT INTO `zz_mobilecat` VALUES (1,'最新活动',0,'0','1',0,'','',0,0,'','/content/share',1,''),(2,'十元区',0,'0','2',0,'','',0,0,'','/yunbuy?zq=10',1,''),(3,'限购区',0,'0','3',0,'','',0,0,'','/yunbuy?zq=limit',1,''),(4,'返利榜',0,'0','4',0,'','',0,0,'','/content/share',1,'');
/*!40000 ALTER TABLE `zz_mobilecat` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:19
