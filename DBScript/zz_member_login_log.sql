-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_member_login_log`
--

DROP TABLE IF EXISTS `zz_member_login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_member_login_log` (
  `sesskey` char(32) NOT NULL,
  `mid` int(11) NOT NULL DEFAULT '0',
  `adminid` int(11) NOT NULL DEFAULT '0',
  `ip` char(15) NOT NULL,
  `c_time` int(10) NOT NULL,
  PRIMARY KEY (`sesskey`),
  KEY `sesskey` (`sesskey`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_member_login_log`
--

LOCK TABLES `zz_member_login_log` WRITE;
/*!40000 ALTER TABLE `zz_member_login_log` DISABLE KEYS */;
INSERT INTO `zz_member_login_log` VALUES ('rju42vmdeq4c699re1blhtuss6',0,0,'127.0.0.1',1480432258),('v89rhb2bigodlalin2lq4iehk1',0,0,'127.0.0.1',1480222702),('sk7g09gl9ofaoieco6he7gdv66',0,0,'127.0.0.1',1480247886),('91a3jemo8j7njjt1v0v3ot4sd6',0,0,'127.0.0.1',1480184134),('m134oigsu7nt92bhusfp1863a2',0,0,'127.0.0.1',1480435059),('feg91qefuljq2ed6echjc03sg1',0,0,'127.0.0.1',1480786068),('octt8ld607372riuodqsfmqfi5',0,0,'127.0.0.1',1480779138),('juhgto1o4gum8uopu8i4j91831',0,0,'127.0.0.1',1480902622),('vuigu2qv23o8bv9qaiel7eehj3',0,0,'127.0.0.1',1480957055),('5b95hee803vbhoul59716ifl62',0,0,'127.0.0.1',1481813798),('fl1hnj4dnb0gllocnb46p4rbg6',0,0,'127.0.0.1',1482476123),('61g50j37vaiusdudoavdqii7i0',0,0,'127.0.0.1',1482478101),('golebnaul5k36jdp0sksqck384',0,0,'127.0.0.1',1482573426),('b42pk1ab9kecub11his3fm23j3',0,0,'127.0.0.1',1482909203),('bq14ealipagi1ebvf7p6qkelu1',0,0,'127.0.0.1',1483110838),('752b5kkkhftao2lfg7tu860f66',0,0,'127.0.0.1',1483461281),('p9vtakanv42s9r9urp0ocgcbm0',0,0,'127.0.0.1',1483537688),('afmtm1a6vgdfhb1hpbv8cffts6',0,0,'127.0.0.1',1483539822),('3m0g81a5vh866unho3s90ioe35',0,0,'127.0.0.1',1485015038);
/*!40000 ALTER TABLE `zz_member_login_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:19
