-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_member`
--

DROP TABLE IF EXISTS `zz_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_member` (
  `mid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(60) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `pay_password` varchar(64) DEFAULT NULL COMMENT '支付密码',
  `photo` varchar(255) DEFAULT NULL COMMENT '头像',
  `ivt_id` int(11) NOT NULL DEFAULT '0' COMMENT '邀请人',
  `salt` char(6) DEFAULT NULL,
  `nickname` varchar(30) DEFAULT NULL COMMENT '昵称',
  `realname` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `idcard` varchar(60) DEFAULT NULL,
  `rank_id` smallint(3) NOT NULL DEFAULT '0',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `verify_email` tinyint(2) DEFAULT '0',
  `zone` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `sex` tinyint(2) DEFAULT '1' COMMENT '1男,2女',
  `mobile` varchar(20) DEFAULT NULL,
  `verify_mobile` tinyint(1) DEFAULT '0',
  `rank_points` int(10) NOT NULL DEFAULT '0' COMMENT '等级积分',
  `pay_points` int(10) NOT NULL DEFAULT '0' COMMENT '支付积分',
  `db_points` int(10) DEFAULT '0' COMMENT '淘币',
  `db_scores` int(10) DEFAULT '0' COMMENT '福分',
  `commission` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '佣金余额',
  `deduct_commission` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '扣除佣金',
  `user_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '可用余额',
  `frozen_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '冻结金额',
  `back_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '解冻金额',
  `status` smallint(3) NOT NULL DEFAULT '1' COMMENT '1正常,0关闭',
  `auth` varchar(64) DEFAULT NULL COMMENT '授权码',
  `auth_time` int(11) DEFAULT NULL COMMENT '授权时间',
  `ip` char(15) DEFAULT NULL COMMENT '注册IP',
  `lastip` char(15) NOT NULL COMMENT '上次登陆IP',
  `login_time` smallint(6) DEFAULT '0' COMMENT '登录次数',
  `login` int(11) DEFAULT NULL COMMENT '当前登陆时间',
  `is_voice` smallint(3) DEFAULT '0' COMMENT '是否语音验证',
  `is_perfect` smallint(3) NOT NULL DEFAULT '0' COMMENT '是否完善资料',
  `lastlogin` int(11) unsigned DEFAULT NULL,
  `oauth_login` varchar(255) NOT NULL COMMENT '授权登录',
  `c_time` int(10) DEFAULT NULL,
  `is_robots` smallint(3) NOT NULL DEFAULT '0',
  `intro` varchar(50) DEFAULT NULL,
  `ivt_count` int(8) NOT NULL DEFAULT '0',
  `pay_money` decimal(10,2) DEFAULT '0.00',
  `free` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可以免费',
  `openid` varchar(36) NOT NULL,
  `unionid` varchar(36) NOT NULL,
  `subscribe_time` int(10) NOT NULL,
  `spacedata` int(20) unsigned NOT NULL DEFAULT '0',
  `rechargetimes` int(8) DEFAULT '0' COMMENT '充值次数',
  `is_comregistry` smallint(3) NOT NULL DEFAULT '0' COMMENT '代理注册',
  `comreg_rank_id` int(20) unsigned NOT NULL DEFAULT '1',
  `comreg_points` int(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=2016 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_member`
--

LOCK TABLES `zz_member` WRITE;
/*!40000 ALTER TABLE `zz_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_member` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:23
