-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: xinli_dev
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zz_yuncart`
--

DROP TABLE IF EXISTS `zz_yuncart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_yuncart` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `buy_id` int(10) NOT NULL COMMENT '港淘ID',
  `goods_name` varchar(255) NOT NULL COMMENT '产品名称',
  `cover` int(11) NOT NULL COMMENT '封面ID',
  `mid` int(10) NOT NULL COMMENT '用户ID',
  `qishu` smallint(6) NOT NULL COMMENT '期数',
  `qty` smallint(6) NOT NULL COMMENT '购买数量',
  `multi` smallint(6) DEFAULT '1' COMMENT '多期参与',
  `goods_price` decimal(20,0) DEFAULT '0' COMMENT '商品价格',
  `price` decimal(20,0) NOT NULL COMMENT '单价',
  `subtotal` decimal(20,0) NOT NULL COMMENT '小计',
  `type` smallint(3) DEFAULT '1' COMMENT '类型',
  `prev_buy_id` int(11) DEFAULT '0' COMMENT '过期的港淘ID',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_yuncart`
--

LOCK TABLES `zz_yuncart` WRITE;
/*!40000 ALTER TABLE `zz_yuncart` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_yuncart` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-22  0:30:15
