/*>>>>>timing模型字段*/
var timing=function(){

};
timing.prototype={

    //批量解冻
    frozen:function(){
        var D={};
        $post({
            url:'timing/frozen',
            method:'post',
            data:D,
            callback:function(x){
                if(x != 0){
                    com.xtip(x,{type:1})
                }else{
                    com.xtip('批量解冻完成');
                }
                main.refresh();
            }
        });
    },

    //更新订单统计
    cron:function(){
        $post({
            url:'/timing/ajaxCron',
            data:{ajax:1},
            callback:function(x){
                $('#timingCron').html(x);
            }
        });
    },

    //清理数据库
    clear:function(){
        if(!confirm('确认清理这些数据！')) return;
        var D={};
        $post({
            url:'/timing/clear',
            method:'post',
            data:D,
            callback:function(x){
                if(x != 0){
                    com.xtip(x,{type:1})
                }else{
                    com.xtip('数据清理完成');
                    main.refresh();
                }
            }
        });
    },

    //数据库更新
    updateDb:function(){
        if(!confirm('确认刚升级完网站并更新数据库！')) return;
        var D={};
        $post({
            url:'/databases/updateDb',
            method:'post',
            dataType:'json',
            data:D,
            callback:function(x){
                com.xtip(x.html,{type: x.type})
            }
        });
    }

};timing = new timing;